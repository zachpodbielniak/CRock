# CRock

C Micro-Framework Library Similar To Python's Flask.
Now Supports FreeBSD!

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

CRock is licensed under the AGPLv3 license. Don't like it? Go else where.

CRock utilizes my [PodNet](https://gitlab.com/zachpodbielniak/PodNet) library, which is a general purpose C library for making life easier.

## C!? REST API!? WHAT!?
Don't panic, its super simple to use, and because its a C REST API implementation, it makes it super simple to integrate with a lot of other things. 

### Example
```
HTTP_STATUS_CODE
MyHomePage(
	HANDLE			hHttpServer,
	LPCSTR RESTRICT 	lpcszUri,
	HTTP_METHOD 		hmType,
	LPVOID 			lpUserData,
	LPREQUEST_DATA		lprdData
){
	/* Add A Header To State This Is PlainText */
	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");

	/* Set The Response Body To "Hello World!\n" */
	RequestAddResponse(lprdData->hRequest, "Hello World!\n", StringLength("Hello World!\n"));

	/* Return 200 OK */
	return HTTP_STATUS_200_OK;
}


int
main(
	int			argc,
	char 			**argv
){
	HANDLE hHttpServer;
	DLPSTR dlpszBindAddresses; /* Supports Multiple Binding Addresses */
	LPUSHORT lpusBindPorts;    /* Ports For The Above Addresses */

	dlpszBindAddresses = malloc(sizeof(LPSTR) * 2);
	lpusBindPorts = malloc(sizeof(USHORT) * 2);

	dlpszBindAddresses[0] = "10.0.0.40"; /* IPv4 Address */
	dlpszBindAddresses[1] = "2001:470:c035::40"; /* IPv6 Address Support */

	lpusBindPorts[0] = 9998; /* Per Address Port Binding */
	lpusBindPorts[1] = 9999; 

	/* Create Our HTTP Server Instance */
	hHttpServer = CreateHttpServer(
		"MyWebServer",
		(DLPCSTR)dlpszBindAddresses,
		2,
		lpusBindPorts,
		2,
		"mywebserver",
		65536
	);

	/* Define Our API Endpoint */
	DefineHttpUriProc(
		hHttpServer,		/* Server Instance */
		"/",			/* URI Path */
		HTTP_METHOD_GET,	/* Method Type */
		MyHomePage,		/* CallBack Function */
		NULLPTR			/* UserData For CallBack Function */
	);

	/* Wait For HTTP Server To Return */ 
	WaitForSingleObject(hHttpServer, INFINITE_WAIT_TIME);
	return 0;
}
```

### How Does It Work?
Under the hood, CRock is semi-multithreading intensive. Each binding address configured gets its own thread, which accepts new connections on. Each one of those connections is then added to an epoll list. There is an epoll event listener that runs in its own thread as well, and on a new event, that connection is added to a work queue to be processed. There are multiple worker threads that pull jobs off of that work queue to be processed then. At a bare minimum, there will be 3 threads, one for the bind listener, one for epoll, and one for the worker.

### What Should I Use CRock For?
Whatever you want! Whether you like to program everything in C, or you simply want an easy way to expose a C or C-Compatible library as a REST API over HTTP. CRock can solve a lot of various problems.

As well, paired with another project of mine, [PodMQ](https://gitlab.com/zachpodbielniak/PodMQ), you could easily build out a large microservices framework in just C. Add on top of that, adding my automation tool, [Podomation](https://gitlab.com/zachpodbielniak/Podomation), you can solve a lot of various problems, with just C, in the modern cloud environment.

There is no need to learn a fancy new language to take advantage of new tools like Docker. Just use C, and show off your skills.

### What CRock Doesn't Do
TLS / SSL. CRock was designed to be put behind a reverse proxy, like Nginx. As such, CRock does not need to implement TLS off the bad. However, this may be added in the future.

## Donate
Like CRock? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Docker
There will be a pre-built Docker image in the future. As of right now, just build the one in this repo.

```
docker build -t crock .
docker run -d --name crock crock
curl http://172.17.0.2:9998/
```