/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Src/CRock.h"

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif




_Call_Back_
HTTP_STATUS_CODE
__Root(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	LPQUERYSTRING_DATA lpqsdInfo;
	UARCHLONG ualIndex;
	CSTRING csBody[8192];
	CSTRING csTemp[512];

	LPSTR lpszBegin;
	LPSTR lpszHeaderEnd;
	LPSTR lpszEnd;
	#ifdef BUILD_WITH_ONEAGENT_SDK
	HSTRING hsRum;
	#endif

	ZeroMemory(csBody, sizeof(csBody));
	ZeroMemory(csTemp, sizeof(csTemp));

	PrintFormat("/ was called\n");
	PrintFormat("QueryString: %s\n", lprdData->lpszRequestParams);
	ProcessQueryStringParameters(lprdData->hRequest);

	lpszBegin = "<html><head><title>CRock Homepage</title>";
	lpszHeaderEnd = "</head><body>";
	lpszEnd = "</body></html>";

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>%s</h1><br /><h4>%s</h4><br /><br />",
			lpqsdInfo->lpszKey,
			lpqsdInfo->lpszValue
		);

		StringConcatenateSafe(csBody, csTemp, sizeof(csBody) - 1);
	}


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");


	RequestAddResponse(lprdData->hRequest, lpszBegin, StringLength(lpszBegin));
	#ifdef BUILD_WITH_ONEAGENT_SDK
	hsRum = GetRumScriptTag(lprdData->hHttpServer);
	RequestAddResponse(lprdData->hRequest, (LPVOID)HeapStringValue(hsRum), HeapStringLength(hsRum));
	DestroyHeapString(hsRum);
	#endif
	RequestAddResponse(lprdData->hRequest, lpszHeaderEnd, StringLength(lpszHeaderEnd));
	RequestAddResponse(lprdData->hRequest, csBody, StringLength(csBody));
	RequestAddResponse(lprdData->hRequest, lpszEnd, StringLength(lpszEnd));
	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__RootPost(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	UARCHLONG ualIndex;
	CSTRING csResponse[8192];
	CSTRING csTemp[512];

	ZeroMemory(csResponse, sizeof(csResponse));

	PrintFormat("/ POST was called\n");
	RequestAddHeader(lprdData->hRequest, "X-Multipart-Form: TRUE");
	RequestAddHeader(lprdData->hRequest, "Content-Type: application/json");

	PrintFormat("Is Multipart: %d\n", PostIsMultipartForm(lprdData->hRequest));
	ProcessMultipartForm(lprdData->hRequest);

	StringConcatenateSafe(csResponse, "[", sizeof(csResponse));

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvFormData);
		ualIndex++
	){
		LPFORM_DATA lpfdInfo;

		ZeroMemory(csTemp, sizeof(csTemp));
		lpfdInfo = (LPFORM_DATA)VectorAt(lprdData->hvFormData, ualIndex);
		
		if (0 != ualIndex)
		{ 
			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp) - 1,
				",{\"key\": \"%s\", \"value\": \"%s\"}",
				lpfdInfo->lpszKey,
				lpfdInfo->lpszValue
			);
		}
		else
		{
			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp) - 1,
				"{\"key\": \"%s\", \"value\": \"%s\"}",
				lpfdInfo->lpszKey,
				lpfdInfo->lpszValue
			);
		}
		
		StringConcatenateSafe(csResponse, csTemp, sizeof(csResponse));
	}

	StringConcatenateSafe(csResponse, "]", sizeof(csResponse));

	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));
	return HTTP_STATUS_200_OK;
}




_Call_Back_
HTTP_STATUS_CODE
__Info(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);
	UNREFERENCED_PARAMETER(lprdData);

	PrintFormat("/Info was called\n"); 
	return HTTP_STATUS_502_BAD_GATEWAY;
}




_Call_Back_
HTTP_STATUS_CODE
__Decode(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	LPQUERYSTRING_DATA lpqsdInfo;
	UARCHLONG ualIndex;
	CSTRING csBody[8192];
	CSTRING csTemp[512];

	LPSTR lpszBegin;
	LPSTR lpszHeaderEnd;
	LPSTR lpszEnd;
	#ifdef BUILD_WITH_ONEAGENT_SDK
	HSTRING hsRum;
	#endif

	UrlDecode(lprdData->hRequest);

	ZeroMemory(csBody, sizeof(csBody));
	ZeroMemory(csTemp, sizeof(csTemp));

	PrintFormat("/Decode was called\n");
	PrintFormat("QueryString: %s\n", lprdData->lpszRequestParams);
	PrintFormat("QueryStringDecoded: %s\n", lprdData->lpszRequestParamsDecoded);
	PrintFormat("Number of Objects: %llu\n", GetTotalNumberOfObjects());
	ProcessQueryStringParameters(lprdData->hRequest);

	lpszBegin = "<html><head><title>CRock Decode Test</title>";
	lpszHeaderEnd = "</head><body>";
	lpszEnd = "</body></html>";

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>%s</h1><br /><h4>%s</h4><br /><br />",
			lpqsdInfo->lpszKey,
			lpqsdInfo->lpszValue
		);

		StringConcatenateSafe(csBody, csTemp, sizeof(csBody) - 1);
	}


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");


	RequestAddResponse(lprdData->hRequest, lpszBegin, StringLength(lpszBegin));
	#ifdef BUILD_WITH_ONEAGENT_SDK
	hsRum = GetRumScriptTag(lprdData->hHttpServer);
	RequestAddResponse(lprdData->hRequest, (LPVOID)HeapStringValue(hsRum), HeapStringLength(hsRum));
	DestroyHeapString(hsRum);
	#endif
	RequestAddResponse(lprdData->hRequest, lpszHeaderEnd, StringLength(lpszHeaderEnd));
	RequestAddResponse(lprdData->hRequest, csBody, StringLength(csBody));
	RequestAddResponse(lprdData->hRequest, lpszEnd, StringLength(lpszEnd));
	return HTTP_STATUS_200_OK;
}



LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hHttpServer;
	DLPSTR dlpszBindAddresses;
	LPUSHORT lpusBindPorts;
	LPSTATIC_OPTIONS lpsoOptions;

	dlpszBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));
	lpusBindPorts = GlobalAllocAndZero(sizeof(USHORT));

	dlpszBindAddresses[0] = "0.0.0.0";
	lpusBindPorts[0] = 9998;

	lpsoOptions = CreateStaticContentOptions(
		NULLPTR,
		"/Static/"
	);

	PrintFormat("Number of Objects: %lu\n", GetTotalNumberOfObjects());


	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_result_t orInit;

	orInit = onesdk_initialize_2(0);
	if (ONESDK_SUCCESS != orInit)
	{ PostQuitMessage(254); }
	
	hHttpServer = CreateHttpServerExOneSdk(
		"CRock",
		(DLPCSTR)dlpszBindAddresses,
		1,
		lpusBindPorts,
		1,
		"crock.podbielniak.com",
		65536,
		2,
		"/",
		NULLPTR,
		443,
		NULLPTR,
		NULLPTR,
		30,
		0
	);

	#else

	hHttpServer = CreateHttpServerEx(
		"testsrv01",
		(DLPCSTR)dlpszBindAddresses,
		1,
		lpusBindPorts,
		1,
		"testsrv01",
		65536,
		1,
		0
	);
	#endif

	PrintFormat("Number of Objects: %lu\n", GetTotalNumberOfObjects());


	DefineHttpUriProc(
		hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__Root,
		NULLPTR
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/",
		HTTP_METHOD_POST,
		__RootPost,
		NULLPTR
	);

	DefineHttpUriProc(
		hHttpServer,
		"/Info",
		HTTP_METHOD_GET,
		__Info,
		NULLPTR
	);

	/*
	DefineHttpUriProc(
		hHttpServer,
		"/*",
		HTTP_METHOD_GET,
		__Root,
		NULLPTR
	);
	*/

	DefineHttpUriProc(
		hHttpServer,
		"/Test/*",
		HTTP_METHOD_GET | HTTP_METHOD_POST,
		__Info,
		NULLPTR
	);

	DefineHttpUriProc(
		hHttpServer,
		"/Decode",
		HTTP_METHOD_GET | HTTP_METHOD_POST,
		__Decode,
		NULLPTR
	);

	DefineHttpUriProc(
		hHttpServer,
		"/Static",
		HTTP_METHOD_GET,
		StaticContentServer,
		lpsoOptions
	);
	
	DefineHttpUriProc(
		hHttpServer,
		"/Static/*",
		HTTP_METHOD_GET,
		StaticContentServer,
		lpsoOptions
	);

	PrintFormat("Number of Objects: %lu\n", GetTotalNumberOfObjects());
	LongSleep(INFINITE_WAIT_TIME);
	return 0;
}