/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Src/CRock.h"



typedef struct __STATE
{
	HANDLE 			hHttpServer;
	LPSTATIC_OPTIONS	lpsoOptions;

	DLPSTR 			dlpszHttpBindAddresses;
	LPUSHORT 		lpusHttpBindPorts;

	LPSTR 			lpszUri;
	LPSTR			lpszUriWild;
	LPSTR 			lpszDir;

	LONG 			lArgCount;
	DLPSTR 			dlpszArgValues;
} STATE, *LPSTATE;




INTERNAL_OPERATION
KILL
__PrintHelp(
	VOID 
){
	PrintFormat("CRock -- StaticContentServer\n");
	PrintFormat("----------------------------\n");
	PrintFormat("CRock is licensed under the AGPLv3 license by Zach Podbielniak\n");
	PrintFormat("Source: https://gitlab.com/zachpodbielniak/CRock/\n\n");
	PrintFormat("Usage:\tstatic-web-server [options]\n");
	PrintFormat("\t-h,--help\tPrint this message\n");
	PrintFormat("\t-l,--license\tPrint License Info\n");
	PrintFormat("\t-p,--port\tSpecify HTTP Port (default 9998)\n");
	PrintFormat("\t-b,--bind\tSpecify bind address (default 127.0.0.1)\n");
	PrintFormat("\t-u,--uri\tSpecify the URI at which to register the StaticContentServer\n");
	PrintFormat("\t-d,--dir\tSpecify the directory to serve\n");

	PostQuitMessage(0);
}




INTERNAL_OPERATION
KILL 
__PrintLicense(
	VOID
){
	PrintFormat("  ____ ____            _    \n");
	PrintFormat(" / ___|  _ \\ ___   ___| | __\n");
	PrintFormat("| |   | |_) / _ \\ / __| |/ /\n");
	PrintFormat("| |___|  _ < (_) | (__|   < \n");
	PrintFormat(" \\____|_| \\_\\___/ \\___|_|\\_\\\n");
	PrintFormat("C Micro-Framework Library Similar To Python's Flask.\n");
	PrintFormat("Copyright (C) 2019 Zach Podbielniak\n");
	PrintFormat("\n");
	PrintFormat("This program is free software: you can redistribute it and/or modify\n");
	PrintFormat("it under the terms of the GNU Affero General Public License as\n");
	PrintFormat("published by the Free Software Foundation, either version 3 of the\n");
	PrintFormat("License, or (at your option) any later version.\n");
	PrintFormat("\n");
	PrintFormat("This program is distributed in the hope that it will be useful,\n");
	PrintFormat("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	PrintFormat("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
	PrintFormat("GNU Affero General Public License for more details.\n");
	PrintFormat("\n");
	PrintFormat("You should have received a copy of the GNU Affero General Public License\n");
	PrintFormat("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");

	PostQuitMessage(0);
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ParseArgs(
	_In_ 		LPSTATE 	lpstProgram
){
	UARCHLONG ualIndex;
	LPSTR lpszKey, lpszValue;

	if (1 == lpstProgram->lArgCount)
	{ return TRUE; }

	for (
		ualIndex = 1;
		ualIndex < (UARCHLONG)lpstProgram->lArgCount;
		ualIndex++
	){
		lpszKey = lpstProgram->dlpszArgValues[ualIndex];
		lpszValue = NULLPTR;

		if (ualIndex <= (UARCHLONG)(lpstProgram->lArgCount - 1))
		{ lpszValue = lpstProgram->dlpszArgValues[ualIndex + 1]; }

		if (
			0 == StringCompare("-h", lpszKey) ||
			0 == StringCompare("--help", lpszKey)
		){ __PrintHelp(); }

		if (
			0 == StringCompare("-l", lpszKey) ||
			0 == StringCompare("--license", lpszKey)
		){ __PrintLicense(); }

		else if (
			0 == StringCompare("-b", lpszKey) ||
			0 == StringCompare("--bind", lpszKey)
		){
			if (NULLPTR == lpszValue)
			{ continue; }

			lpstProgram->dlpszHttpBindAddresses[0] = StringDuplicate(lpszValue);
		}
		
		else if (
			0 == StringCompare("-p", lpszKey) || 
			0 == StringCompare("--port", lpszKey)
		){
			if (NULLPTR == lpszValue)
			{ continue; }

			StringScanFormat(
				lpszValue,
				"%hu",
				&(lpstProgram->lpusHttpBindPorts[0])
			);
		}

		else if (
			0 == StringCompare("-u", lpszKey) ||
			0 == StringCompare("--uri", lpszKey)
		){ lpstProgram->lpszUri = lpszValue; }

		else if (
			0 == StringCompare("-d", lpszKey) ||
			0 == StringCompare("--dir", lpszKey)
		){ lpstProgram->lpszDir = lpszValue; }
		
		else
		{ continue; }		
	}

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_ 		LONG		lArgCount,
	_In_Z_		DLPSTR		dlpszArgValues
){
	STATE stProgram;
	ARCHLONG alSize;

	ZeroMemory(&stProgram, sizeof(stProgram));
	stProgram.lArgCount = lArgCount;
	stProgram.dlpszArgValues = dlpszArgValues;
	stProgram.dlpszHttpBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));	
	stProgram.lpusHttpBindPorts = GlobalAllocAndZero(sizeof(SHORT));

	stProgram.dlpszHttpBindAddresses[0] = "127.0.0.1";
	stProgram.lpusHttpBindPorts[0] = 9998;
	stProgram.lpszUri = "/";

	__ParseArgs(&stProgram);

	alSize = StringLength(stProgram.lpszUri);
	alSize += 4;
	stProgram.lpszUriWild = GlobalAllocAndZero(alSize);

	if (StringEndsWith(stProgram.lpszUri, "/"))
	{
		StringPrintFormatSafe(
			stProgram.lpszUriWild,
			alSize - 1,
			"%s*",
			stProgram.lpszUri
		);
	}
	else 
	{
		StringPrintFormatSafe(
			stProgram.lpszUriWild,
			alSize - 1,
			"%s/*",
			stProgram.lpszUri
		);
	}

	stProgram.hHttpServer = CreateHttpServerEx(
		"StaticContentServer",
		(DLPCSTR)stProgram.dlpszHttpBindAddresses,
		1,
		stProgram.lpusHttpBindPorts,
		1,
		"StaticContentServer",
		1024,
		4,
		0
	);

	stProgram.lpsoOptions = CreateStaticContentOptions(
		stProgram.lpszDir,
		stProgram.lpszUri
	);

	stProgram.lpsoOptions->bListDirectoryContents = TRUE;

	if (NULLPTR == stProgram.lpsoOptions)
	{
		WriteFile(GetStandardError(), "LPSTATIC_OPTIONS object is NULL. Make sure you follow -f and -m correctly!\n", 0);
		PostQuitMessage(1);
	}

	DefineHttpUriProc(
		stProgram.hHttpServer,
		stProgram.lpszUri,
		HTTP_METHOD_GET,
		StaticContentServer,
		stProgram.lpsoOptions
	);
	
	DefineHttpUriProc(
		stProgram.hHttpServer,
		stProgram.lpszUriWild,
		HTTP_METHOD_GET,
		StaticContentServer,
		stProgram.lpsoOptions
	);

	INFINITE_LOOP()
	{ LongSleep(INFINITE_WAIT_TIME); }

	return 0;		
}
