/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Server.h"
#include "HttpMethod.c"

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif







typedef struct __HTTP_SERVER
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HANDLE 			hQueueLock;
	HANDLE 			hPoller;
	HANDLE 			hDispatchThread;

	HVECTOR 		hvListenThreads;
	HVECTOR 		hvListenSockets;
	HVECTOR 		hvProcessingThreads;
	HQUEUE			hqJobs;

	HHASHTABLE		hhtUris;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPreRequestHook;
	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPostRequestHook;
	LPVOID 			lpPreRequestHookParam;
	LPVOID 			lpPostRequestHookParam;

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_webapplicationinfo_handle_t	owihOneWebApp;
	LPSTR 			lpszContextRoot;
	LPSTR 			lpszTenantHostname;
	USHORT 			usTenantPort;
	LPSTR 			lpszApiToken;
	LPSTR 			lpszApplicationId;
	UARCHLONG 		ualRumRefreshTime;
	HANDLE 			hRumRefreshThread;
	LPSTR 			lpszRumScript;
	#endif
} HTTP_SERVER, *LPHTTP_SERVER;




typedef struct __HTTP_SERVER_LISTEN_ARG
{
	LPHTTP_SERVER 		lphsServer;
	UARCHLONG 		ualAddressIndex;
} HTTP_SERVER_LISTEN_ARG, *LPHTTP_SERVER_LISTEN_ARG;



typedef struct __HTTP_QUEUE_JOB
{
	HANDLE 			hClient;
	HANDLE 			hRequestRegion;
} HTTP_QUEUE_JOB, *LPHTTP_QUEUE_JOB;




_Thread_Proc_
INTERNAL_OPERATION
LPVOID
__JobHandler(
	_In_Opt_ 	LPVOID 			lpParam
){
	LPHTTP_SERVER lphsServer;
	HQUEUE hJobs;
	LPHTTP_QUEUE_JOB lphqjInfo;
	HANDLE hRequest;

	lphsServer = (LPHTTP_SERVER)lpParam;
	hJobs = lphsServer->hqJobs;
	
	/* Pull Each Job Off The Queue And Then Process */
	INFINITE_LOOP()
	{
		__LOOP:
		lphqjInfo = NULLPTR;
		hRequest = NULL_OBJECT;

		WaitForSingleObject(lphsServer->hQueueLock, INFINITE_WAIT_TIME);
		lphqjInfo = QueueNextInLine(hJobs);
		ReleaseSingleObject(lphsServer->hQueueLock);

		if (NULLPTR == lphqjInfo)
		{
			Sleep(1);
			JUMP(__LOOP);
		}

		if (NULL_OBJECT == lphqjInfo->hClient || NULL_OBJECT == lphqjInfo->hRequestRegion)
		{
			FreeMemory(lphqjInfo);
			Sleep(1);
			JUMP(__LOOP);
		}
		/*
		PrintFormat("There was a job!\n");
		PrintFormat("%s\n", DynamicHeapRegionData(lphqjInfo->hRequestRegion, NULLPTR));
		*/

		/* Handle The Work */
		hRequest = CreateRequest(
			lphsServer->hThis,
			lphqjInfo->hClient,
			lphsServer->hhtUris,
			lphqjInfo->hRequestRegion
		);

		if (NULL_OBJECT == hRequest)
		{
			DestroyObject(lphqjInfo->hRequestRegion);
			FreeMemory(lphqjInfo);
			JUMP(__LOOP);
		}

		HandleRequest(hRequest);
		
		DestroyObject(hRequest);
		RemoveFileFromPoller(lphsServer->hPoller, lphqjInfo->hClient);
		DestroyObject(lphqjInfo->hClient);
		FreeMemory(lphqjInfo);
	}

	return NULLPTR;
}



_Thread_Proc_
INTERNAL_OPERATION
LPVOID 
__DispatchProc(
	_In_Opt_ 	LPVOID 			lpParam
){
	LPHTTP_SERVER lphsServer;
	HANDLE hPoller;
	LPPOLL_DESCRIPTOR lppdInfo;
	UARCHLONG ualNumberOfClients;
	UARCHLONG ualIndex;
	LPSTR lpszTemp;
	BOOL bWait;

	lphsServer = (LPHTTP_SERVER)lpParam;
	hPoller = lphsServer->hPoller;
	lpszTemp = LocalAllocAndZero(CROCK_SERVER_DEFAULT_REQUEST_REGION);

	INFINITE_LOOP()
	{
		ZeroMemory(lpszTemp, CROCK_SERVER_DEFAULT_REQUEST_REGION);
		bWait = WaitForSingleObject(hPoller, 1);

		if (FALSE == bWait)
		{ 
			Sleep(1);
			continue;
		}
		
		ualNumberOfClients = GetPollResultEx(hPoller, &lppdInfo);

		for (
			ualIndex = 0;
			ualIndex < ualNumberOfClients;
			ualIndex++
		){
			HANDLE hClient;
			UARCHLONG ualBytes;
			ARCHLONG alRead;
			HTTP_QUEUE_JOB hqjInfo;


			hClient = NULL_OBJECT;
			ualBytes = 0;
			alRead = 0;

			hClient = lppdInfo[ualIndex].hFile;
			hqjInfo.hClient = hClient;

			if (NULL_OBJECT == hClient)
			{ continue; }

			/* IPv6 */
			if (HANDLE_TYPE_SOCKET6 == GetHandleDerivativeType(hClient))
			{ ualBytes = SocketBytesInQueue6(hClient); }
			else 
			{ ualBytes = SocketBytesInQueue4(hClient); }

			if (0 != ualBytes)
			{
				hqjInfo.hRequestRegion = CreateDynamicHeapRegion(
					NULL_OBJECT,
					ualBytes,
					FALSE,
					0
				);
				
				if (HANDLE_TYPE_SOCKET6 == GetHandleDerivativeType(hClient))
				{ 
					while (-1 != (alRead += SocketReceive6(hClient, lpszTemp, CROCK_SERVER_DEFAULT_REQUEST_REGION)))
					{ DynamicHeapRegionAppend(hqjInfo.hRequestRegion, lpszTemp, alRead); }
				}
				else
				{ 
					while (0 != ualBytes)
					{ 
						alRead = SocketReceive4(hClient, lpszTemp, CROCK_SERVER_DEFAULT_REQUEST_REGION);
						DynamicHeapRegionAppend(hqjInfo.hRequestRegion, lpszTemp, alRead); 
						ualBytes -= alRead;
					}
				}

				WaitForSingleObject(lphsServer->hQueueLock, INFINITE_WAIT_TIME); 
				QueuePush(lphsServer->hqJobs, &hqjInfo); 
				ReleaseSingleObject(lphsServer->hQueueLock);
			}
			else
			{ 
				RemoveFileFromPoller(hPoller, hClient);
				/* DestroyObject(hClient); */
			}
		}


		DestroyPollResult(hPoller);
	}


	return NULLPTR;
}




_Thread_Proc_
INTERNAL_OPERATION
LPVOID 
__ListenThread(
	_In_Opt_ 	LPVOID 			lpParam
){
	LPHTTP_SERVER_LISTEN_ARG lphslaData;
	HANDLE hSocket;
	HANDLE hClient;
	BOOL bV6;

	lphslaData = (LPHTTP_SERVER_LISTEN_ARG)lpParam;
	hSocket = *(LPHANDLE)VectorAt(
		lphslaData->lphsServer->hvListenSockets,
		lphslaData->ualAddressIndex
	);

	if (HANDLE_TYPE_SOCKET6 == GetHandleDerivativeType(hSocket))
	{ bV6 = TRUE; }
	else
	{ bV6 = FALSE; }
	

	INFINITE_LOOP()
	{
		hClient = NULL_OBJECT;

		if (FALSE == bV6)
		{ hClient = AcceptConnection4(hSocket); }
		else 
		{ hClient = AcceptConnection6(hSocket); }

		if (NULL_OBJECT != hClient)
		{ AddFileToPoller(lphslaData->lphsServer->hPoller, hClient); }
		
	}
	return NULLPTR;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyHttpServer(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPHTTP_SERVER lphsServer;
	UARCHLONG ualIndex;

	lphsServer = (LPHTTP_SERVER)hDerivative;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lphsServer->hvListenSockets);
		ualIndex++
	){ DestroyObject(*(LPHANDLE)VectorAt(lphsServer->hvListenSockets, ualIndex)); }
	
	for (
		ualIndex = 0;
		ualIndex < VectorSize(lphsServer->hvListenThreads);
		ualIndex++
	){ DestroyObject(*(LPHANDLE)VectorAt(lphsServer->hvListenThreads, ualIndex)); }

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lphsServer->hvProcessingThreads);
		ualIndex++
	){ DestroyObject(*(LPHANDLE)VectorAt(lphsServer->hvProcessingThreads, ualIndex)); }

	DestroyQueue(lphsServer->hqJobs);
	DestroyObject(lphsServer->hQueueLock);
	DestroyObject(lphsServer->hPoller);
	DestroyVector(lphsServer->hvListenThreads);
	DestroyVector(lphsServer->hvListenSockets);
	DestroyHashTable(lphsServer->hhtUris);
	DestroyObject(lphsServer->hLock);
	FreeMemory(lphsServer);

	return TRUE;
}





_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServer(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize
){
	UNREFERENCED_PARAMETER(ualNumberOfPorts); /* TODO */

	EXIT_IF_UNLIKELY_NULL(lpcszName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(dlpcszBindAddresses, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpusPorts, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHostName, NULL_OBJECT);

	HANDLE hServer;
	HANDLE hJobThread;
	LPHTTP_SERVER lphsServer;
	UARCHLONG ualIndex;

	if (0 == ualHashSize)
	{ ualHashSize = CROCK_SERVER_DEFAULT_URI_HASH_SIZE; }

	lphsServer = (LPHTTP_SERVER)GlobalAllocAndZero(sizeof(HTTP_SERVER));
	if (NULLPTR == lphsServer)
	{ return NULL_OBJECT; }

	lphsServer->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hLock)
	{
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hhtUris = CreateHashTable(ualHashSize);
	if (NULLPTR == lphsServer->hhtUris)
	{
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenSockets = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenSockets)
	{
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenThreads = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenThreads)
	{
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvProcessingThreads = CreateVector(1, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvProcessingThreads)
	{
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	hServer = CreateHandleWithSingleInheritor(
		lphsServer,
		&(lphsServer->hThis),
		HANDLE_TYPE_HTTP_SERVER,
		__DestroyHttpServer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphsServer->ullId),
		0
	);

	if (NULL_OBJECT == hServer)
	{
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}
	
	lphsServer->hPoller = CreatePoller();
	if (NULL_OBJECT == lphsServer->hPoller)
	{
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hQueueLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hQueueLock)
	{
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hqJobs = CreateQueue(sizeof(HTTP_QUEUE_JOB), NULL_OBJECT);
	if (NULLPTR == lphsServer->hqJobs)
	{
		DestroyObject(lphsServer->hQueueLock);
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	for (
		ualIndex = 0;
		ualIndex < ualNumberOfAddresses;
		ualIndex++
	){
		HANDLE hSocket;
		HANDLE hThread;
		LPHTTP_SERVER_LISTEN_ARG lphslaData;

		hSocket = NULL_OBJECT;
		hThread = NULL_OBJECT;
		lphslaData = NULLPTR;
		
		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			IPV6_ADDRESS ip6Address;
			InitializeIpv6Address(&ip6Address, dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress6(lpusPorts[ualIndex], &ip6Address);
		}
		/* IPv4 */
		else 
		{
			IPV4_ADDRESS ip4Address;
			ip4Address = CreateIpv4AddressFromString(dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress4(lpusPorts[ualIndex], ip4Address);
		}

		/* Check hSocket Value */
		if (NULL_OBJECT == hSocket)
		{
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			BindOnSocket6(hSocket);
			ListenOnBoundSocket6(hSocket, 0);
		}
		else 
		{
			BindOnSocket4(hSocket);
			ListenOnBoundSocket4(hSocket, 0);
		}

		VectorPushBack(lphsServer->hvListenSockets, &hSocket, 0);
		
		lphslaData = GlobalAllocAndZero(sizeof(HTTP_SERVER_LISTEN_ARG));
		if (NULLPTR == lphslaData)
		{
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		lphslaData->lphsServer = lphsServer;
		lphslaData->ualAddressIndex = ualIndex;

		hThread = CreateThread(__ListenThread, (LPVOID)lphslaData, NULLPTR);
		if (NULL_OBJECT == hThread)
		{
			FreeMemory(lphslaData);
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}
		SetThreadName(hThread, "HttpListener");
	}

	#ifdef BUILD_WITH_ONEAGENT_SDK
	lphsServer->owihOneWebApp = onesdk_webapplicationinfo_create(
		onesdk_asciistr(lpcszHostName),
		onesdk_asciistr(lpcszName),
		onesdk_asciistr("/")
	);
	#endif
	
	lphsServer->hDispatchThread = CreateThread(__DispatchProc, lphsServer, NULLPTR);
	SetThreadName(lphsServer->hDispatchThread, "HttpDispatcher");
	
	hJobThread = CreateThread(__JobHandler, lphsServer, 0);
	SetThreadName(hJobThread, "HttpWorker-000");
	VectorPushBack(lphsServer->hvProcessingThreads, &hJobThread, 0);
	return hServer;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServerEx(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize,
	_In_ 		UARCHLONG 		ualNumberOfJobThreads,
	_In_ 		ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ualNumberOfPorts); /* TODO */
	UNREFERENCED_PARAMETER(ulFlags); 

	EXIT_IF_UNLIKELY_NULL(lpcszName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(dlpcszBindAddresses, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpusPorts, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHostName, NULL_OBJECT);

	HANDLE hServer;
	HANDLE hJobThread;
	LPHTTP_SERVER lphsServer;
	UARCHLONG ualIndex;

	if (0 == ualHashSize)
	{ ualHashSize = CROCK_SERVER_DEFAULT_URI_HASH_SIZE; }

	lphsServer = (LPHTTP_SERVER)GlobalAllocAndZero(sizeof(HTTP_SERVER));
	if (NULLPTR == lphsServer)
	{ return NULL_OBJECT; }

	lphsServer->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hLock)
	{
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hhtUris = CreateHashTable(ualHashSize);
	if (NULLPTR == lphsServer->hhtUris)
	{
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenSockets = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenSockets)
	{
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenThreads = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenThreads)
	{
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvProcessingThreads = CreateVector(1, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvProcessingThreads)
	{
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	hServer = CreateHandleWithSingleInheritor(
		lphsServer,
		&(lphsServer->hThis),
		HANDLE_TYPE_HTTP_SERVER,
		__DestroyHttpServer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphsServer->ullId),
		0
	);

	if (NULL_OBJECT == hServer)
	{
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}
	
	lphsServer->hPoller = CreatePoller();
	if (NULL_OBJECT == lphsServer->hPoller)
	{
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hQueueLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hQueueLock)
	{
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hqJobs = CreateQueue(sizeof(HTTP_QUEUE_JOB), NULL_OBJECT);
	if (NULLPTR == lphsServer->hqJobs)
	{
		DestroyObject(lphsServer->hQueueLock);
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	for (
		ualIndex = 0;
		ualIndex < ualNumberOfAddresses;
		ualIndex++
	){
		HANDLE hSocket;
		HANDLE hThread;
		LPHTTP_SERVER_LISTEN_ARG lphslaData;

		hSocket = NULL_OBJECT;
		hThread = NULL_OBJECT;
		lphslaData = NULLPTR;
		
		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			IPV6_ADDRESS ip6Address;
			InitializeIpv6Address(&ip6Address, dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress6(lpusPorts[ualIndex], &ip6Address);
		}
		/* IPv4 */
		else 
		{
			IPV4_ADDRESS ip4Address;
			ip4Address = CreateIpv4AddressFromString(dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress4(lpusPorts[ualIndex], ip4Address);
		}

		/* Check hSocket Value */
		if (NULL_OBJECT == hSocket)
		{
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			BindOnSocket6(hSocket);
			ListenOnBoundSocket6(hSocket, 0);
		}
		else 
		{
			BindOnSocket4(hSocket);
			ListenOnBoundSocket4(hSocket, 0);
		}

		VectorPushBack(lphsServer->hvListenSockets, &hSocket, 0);
		
		lphslaData = GlobalAllocAndZero(sizeof(HTTP_SERVER_LISTEN_ARG));
		if (NULLPTR == lphslaData)
		{
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		lphslaData->lphsServer = lphsServer;
		lphslaData->ualAddressIndex = ualIndex;

		hThread = CreateThread(__ListenThread, (LPVOID)lphslaData, NULLPTR);
		if (NULL_OBJECT == hThread)
		{
			FreeMemory(lphslaData);
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}
	}
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	lphsServer->owihOneWebApp = onesdk_webapplicationinfo_create(
		onesdk_asciistr(lpcszHostName),
		onesdk_asciistr(lpcszName),
		onesdk_asciistr("/")
	);
	#endif

	lphsServer->hDispatchThread = CreateThread(__DispatchProc, lphsServer, NULLPTR);
	
	for (
		ualIndex = 0;
		ualIndex < ualNumberOfJobThreads;
		ualIndex++
	){
		CSTRING csThreadName[16];
		ZeroMemory(csThreadName, sizeof(csThreadName));

		StringPrintFormatSafe(
			csThreadName,
			sizeof(csThreadName) - 0x01U,
			"HttpWorker-%03lu",
			ualIndex
		);

		hJobThread = CreateThread(__JobHandler, lphsServer, 0);
		SetThreadName(hJobThread, (LPCSTR)csThreadName);

		VectorPushBack(lphsServer->hvProcessingThreads, &hJobThread, 0);
	}

	return hServer;
}




#ifdef BUILD_WITH_ONEAGENT_SDK
_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__RumThreadUpdater(
	_In_Opt_ 	LPVOID 			lpParam
){
	EXIT_IF_UNLIKELY_NULL(lpParam, (LPVOID)-1);

	HANDLE hHttpsRequest;
	LPHTTP_SERVER lphsServer;
	CSTRING csRequestUri[1024];
	LPVOID lpBuffer;
	UARCHLONG ualByteWrote;
	LPSTR lpszScript;
	onesdk_tracer_handle_t osthUpdater;

	lphsServer = lpParam;
	ZeroMemory(csRequestUri, sizeof(csRequestUri));
	lpBuffer = LocalAllocAndZero(16384*4);

	if (NULLPTR == lpBuffer)
	{ PostQuitMessage(253); }

	StringPrintFormatSafe(
		csRequestUri,
		sizeof(csRequestUri) - 1,
		"/api/v1/rum/syncCS/%s?Api-Token=%s",
		lphsServer->lpszApplicationId,
		lphsServer->lpszApiToken
	);

	INFINITE_LOOP()
	{
		
		osthUpdater = onesdk_customservicetracer_create(
			onesdk_asciistr("RumThreadUpdater"),
			onesdk_asciistr("RUM Updater Thread")
		);
		
		onesdk_tracer_start(osthUpdater);

		onesdk_customrequestattribute_add_string(
			onesdk_asciistr((LPCSTR)"Tenant"),
			onesdk_asciistr((LPCSTR)lphsServer->lpszTenantHostname)
		);

		onesdk_customrequestattribute_add_string(
			onesdk_asciistr((LPCSTR)"Application ID"),
			onesdk_asciistr((LPCSTR)lphsServer->lpszApplicationId)
		);

		ZeroMemory(lpBuffer, 16384*4);

		hHttpsRequest = CreateHttpsRequest(
			HTTP_REQUEST_GET,
			HTTP_VERSION_1_1,
			lphsServer->lpszTenantHostname,
			443,
			(LPCSTR)csRequestUri,
			HTTP_CONNECTION_CLOSE,
			"CRock RUM Grabber/1.0",
			HTTP_ACCEPT_PLAIN_TEXT,
			FALSE,
			FALSE
		);

		ExecuteHttpsRequest(
			hHttpsRequest,
			lpBuffer,
			16384*4,
			&ualByteWrote
		);

		lpszScript = (LPSTR)lpBuffer;

		WaitForSingleObject(lphsServer->hLock, INFINITE_WAIT_TIME);

		if (NULLPTR != lpszScript && '\0' != *lpszScript && NULLPTR != lphsServer->lpszRumScript)
		{
			if (0 != StringCompare(lpszScript, lphsServer->lpszRumScript))
			{
				UARCHLONG ualLength;

				FreeMemory(lphsServer->lpszRumScript);
				ualLength = StringLength(lpszScript);
				lphsServer->lpszRumScript = GlobalAllocAndZero(ualLength + 0x01U);
				CopyMemory(lphsServer->lpszRumScript, lpszScript, ualLength);
			}
		}
		else
		{
			UARCHLONG ualLength;
			ualLength = StringLength(lpszScript);
			lphsServer->lpszRumScript = GlobalAllocAndZero(ualLength + 0x01U);
			CopyMemory(lphsServer->lpszRumScript, lpszScript, ualLength);
		}
		
		ReleaseSingleObject(lphsServer->hLock);

		DestroyObject(hHttpsRequest);
		onesdk_tracer_end(osthUpdater);
		LongSleep(lphsServer->ualRumRefreshTime);
	}

	return NULLPTR;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__OneSdkExtras(
	_In_ 		LPHTTP_SERVER		lphsServer,
	_In_Z_ 		LPCSTR 			lpcszContextRoot,
	_In_Z_ 		LPCSTR 			lpcszTenantHostname,
	_In_ 		USHORT 			usTenantPort,
	_In_Z_ 		LPCSTR 			lpcszApiToken,
	_In_Z_ 		LPCSTR 			lpcszApplicationId,
	_In_ 		UARCHLONG		ualRumTagRefreshTime
){
	EXIT_IF_UNLIKELY_NULL(lphsServer, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszContextRoot, FALSE);

	if (0 == ualRumTagRefreshTime)
	{ ualRumTagRefreshTime = 5 * 60 * 1000;}

	lphsServer->lpszContextRoot = GlobalAllocAndZero(StringLength(lpcszContextRoot) + 0x01U);
	StringCopy(lphsServer->lpszContextRoot, lpcszContextRoot);

	if (NULLPTR != lpcszTenantHostname)
	{
		lphsServer->lpszTenantHostname = GlobalAllocAndZero(StringLength(lpcszTenantHostname) + 0x01U);
		StringCopy(lphsServer->lpszTenantHostname, lpcszTenantHostname);
	}

	if (NULLPTR != lpcszApiToken)
	{
		lphsServer->lpszApiToken = GlobalAllocAndZero(StringLength(lpcszApiToken) + 0x01U);
		StringCopy(lphsServer->lpszApiToken, lpcszApiToken);
	}

	if (NULLPTR != lpcszApplicationId)
	{
		lphsServer->lpszApplicationId = GlobalAllocAndZero(StringLength(lpcszApplicationId) + 0x01U);
		StringCopy(lphsServer->lpszApplicationId, lpcszApplicationId);
	}

	lphsServer->ualRumRefreshTime = ualRumTagRefreshTime;
	lphsServer->usTenantPort = usTenantPort;
	lphsServer->lpszRumScript = NULLPTR;

	/* Only Spawn RUM Thread If We Want */
	if (NULLPTR != lpcszApiToken && NULLPTR != lpcszApplicationId && NULLPTR != lpcszTenantHostname)
	{
		lphsServer->hRumRefreshThread = CreateThread(__RumThreadUpdater, lphsServer, NULLPTR);
		SetThreadName(lphsServer->hRumRefreshThread, "RumUpdater");
	}

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServerExOneSdk(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize,
	_In_ 		UARCHLONG 		ualNumberOfJobThreads,
	_In_Z_ 		LPCSTR 			lpcszContextRoot,
	_In_Z_ 		LPCSTR 			lpcszTenantHostname,
	_In_ 		USHORT 			usTenantPort,
	_In_Z_ 		LPCSTR 			lpcszApiToken,
	_In_Z_ 		LPCSTR 			lpcszApplicationId,
	_In_ 		UARCHLONG		ualRumTagRefreshTime,
	_In_ 		ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ualNumberOfPorts); /* TODO */
	UNREFERENCED_PARAMETER(ulFlags); 

	EXIT_IF_UNLIKELY_NULL(lpcszName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(dlpcszBindAddresses, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpusPorts, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHostName, NULL_OBJECT);

	HANDLE hServer;
	HANDLE hJobThread;
	LPHTTP_SERVER lphsServer;
	UARCHLONG ualIndex;

	if (0 == ualHashSize)
	{ ualHashSize = CROCK_SERVER_DEFAULT_URI_HASH_SIZE; }

	lphsServer = (LPHTTP_SERVER)GlobalAllocAndZero(sizeof(HTTP_SERVER));
	if (NULLPTR == lphsServer)
	{ return NULL_OBJECT; }

	lphsServer->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hLock)
	{
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hhtUris = CreateHashTable(ualHashSize);
	if (NULLPTR == lphsServer->hhtUris)
	{
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenSockets = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenSockets)
	{
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvListenThreads = CreateVector(ualNumberOfAddresses, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvListenThreads)
	{
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hvProcessingThreads = CreateVector(1, sizeof(HANDLE), 0);
	if (NULLPTR == lphsServer->hvProcessingThreads)
	{
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	hServer = CreateHandleWithSingleInheritor(
		lphsServer,
		&(lphsServer->hThis),
		HANDLE_TYPE_HTTP_SERVER,
		__DestroyHttpServer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphsServer->ullId),
		0
	);

	if (NULL_OBJECT == hServer)
	{
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}
	
	lphsServer->hPoller = CreatePoller();
	if (NULL_OBJECT == lphsServer->hPoller)
	{
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hQueueLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lphsServer->hQueueLock)
	{
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	lphsServer->hqJobs = CreateQueue(sizeof(HTTP_QUEUE_JOB), NULL_OBJECT);
	if (NULLPTR == lphsServer->hqJobs)
	{
		DestroyObject(lphsServer->hQueueLock);
		DestroyObject(lphsServer->hPoller);
		DestroyObject(hServer);
		DestroyVector(lphsServer->hvProcessingThreads);
		DestroyVector(lphsServer->hvListenThreads);
		DestroyVector(lphsServer->hvListenSockets);
		DestroyHashTable(lphsServer->hhtUris);
		DestroyObject(lphsServer->hLock);
		FreeMemory(lphsServer);
		return NULL_OBJECT;
	}

	for (
		ualIndex = 0;
		ualIndex < ualNumberOfAddresses;
		ualIndex++
	){
		HANDLE hSocket;
		HANDLE hThread;
		LPHTTP_SERVER_LISTEN_ARG lphslaData;

		hSocket = NULL_OBJECT;
		hThread = NULL_OBJECT;
		lphslaData = NULLPTR;
		
		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			IPV6_ADDRESS ip6Address;
			InitializeIpv6Address(&ip6Address, dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress6(lpusPorts[ualIndex], &ip6Address);
		}
		/* IPv4 */
		else 
		{
			IPV4_ADDRESS ip4Address;
			ip4Address = CreateIpv4AddressFromString(dlpcszBindAddresses[ualIndex]);
			hSocket = CreateSocketWithAddress4(lpusPorts[ualIndex], ip4Address);
		}

		/* Check hSocket Value */
		if (NULL_OBJECT == hSocket)
		{
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		/* IPv6 */
		if (FALSE == IsIpv4Address(dlpcszBindAddresses[ualIndex]))
		{
			BindOnSocket6(hSocket);
			ListenOnBoundSocket6(hSocket, 0);
		}
		else 
		{
			BindOnSocket4(hSocket);
			ListenOnBoundSocket4(hSocket, 0);
		}

		VectorPushBack(lphsServer->hvListenSockets, &hSocket, 0);
		
		lphslaData = GlobalAllocAndZero(sizeof(HTTP_SERVER_LISTEN_ARG));
		if (NULLPTR == lphslaData)
		{
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}

		lphslaData->lphsServer = lphsServer;
		lphslaData->ualAddressIndex = ualIndex;

		hThread = CreateThread(__ListenThread, (LPVOID)lphslaData, NULLPTR);
		if (NULL_OBJECT == hThread)
		{
			FreeMemory(lphslaData);
			DestroyObject(hSocket);
			DestroyQueue(lphsServer->hqJobs);
			DestroyObject(lphsServer->hQueueLock);
			DestroyObject(lphsServer->hPoller);
			DestroyObject(hServer);
			DestroyVector(lphsServer->hvProcessingThreads);
			DestroyVector(lphsServer->hvListenThreads);
			DestroyVector(lphsServer->hvListenSockets);
			DestroyHashTable(lphsServer->hhtUris);
			DestroyObject(lphsServer->hLock);
			FreeMemory(lphsServer);
			return NULL_OBJECT;
		}
	}
	
	lphsServer->owihOneWebApp = onesdk_webapplicationinfo_create(
		onesdk_asciistr(lpcszHostName),
		onesdk_asciistr(lpcszName),
		onesdk_asciistr(lpcszContextRoot)
	);

	__OneSdkExtras(
		lphsServer,
		lpcszContextRoot,
		lpcszTenantHostname,
		usTenantPort,
		lpcszApiToken,
		lpcszApplicationId,
		ualRumTagRefreshTime
	);

	lphsServer->hDispatchThread = CreateThread(__DispatchProc, lphsServer, NULLPTR);
	
	for (
		ualIndex = 0;
		ualIndex < ualNumberOfJobThreads;
		ualIndex++
	){
		hJobThread = CreateThread(__JobHandler, lphsServer, 0);
		VectorPushBack(lphsServer->hvProcessingThreads, &hJobThread, 0);
	}

	return hServer;
}
#endif




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__AssignCallBack(
	_In_ 		LPHTTP_METHOD_TABLE		lphmtData,
	_In_ 		HTTP_METHOD 			hmType,
	_In_ 		LPFN_HTTP_URI_INVOCATION_PROC	lpfnCallBack,
	_In_		LPVOID 				lpUserData
){
	EXIT_IF_UNLIKELY_NULL(lphmtData, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, FALSE);

	BOOL bSetRealValue;
	bSetRealValue = FALSE;

	if (HTTP_METHOD_GET & hmType)
	{
		lphmtData->lpfnGet = lpfnCallBack;
		lphmtData->lpGetData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_POST & hmType)
	{
		lphmtData->lpfnPost = lpfnCallBack;
		lphmtData->lpPostData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_PUT & hmType)
	{
		lphmtData->lpfnPut = lpfnCallBack;
		lphmtData->lpPutData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_DELETE & hmType)
	{
		lphmtData->lpfnDelete = lpfnCallBack;
		lphmtData->lpDeleteData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_OPTIONS & hmType)
	{
		lphmtData->lpfnOptions = lpfnCallBack;
		lphmtData->lpOptionsData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_HEAD & hmType)
	{
		lphmtData->lpfnHead = lpfnCallBack;
		lphmtData->lpHeadData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_CONNECT & hmType)
	{
		lphmtData->lpfnConnect = lpfnCallBack;
		lphmtData->lpConnectData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_TRACE & hmType)
	{
		lphmtData->lpfnTrace = lpfnCallBack;
		lphmtData->lpTraceData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_PATCH & hmType)
	{
		lphmtData->lpfnPatch = lpfnCallBack;
		lphmtData->lpPatchData = lpUserData;
		bSetRealValue = TRUE;
	}

	if (HTTP_METHOD_UNKNOWN & hmType)
	{ 
		WriteFile(GetStandardError(), "Have HTTP_METHOD_UNKNOWN type\n", 0); 
		bSetRealValue = FALSE;
	}

	return bSetRealValue;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_)
CROCK_API
BOOL
DefineHttpUriProc(
	_In_ 		HANDLE 				hHttpServer,
	_In_Z_		LPCSTR 				lpcszUri,
	_In_ 		HTTP_METHOD			hmType,
	_In_ 		LPFN_HTTP_URI_INVOCATION_PROC	lpfnCallBack,
	_In_ 		LPVOID 				lpUserData
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszUri, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, FALSE);

	LPHTTP_SERVER lphsServer;
	LPHTTP_METHOD_TABLE lphmtData;
	UARCHLONG ualKey;

	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);

	WaitForSingleObject(lphsServer->hLock, INFINITE_WAIT_TIME);

	ualKey = HashTableGetKey(
		lphsServer->hhtUris,
		(ULONGLONG)lpcszUri,
		StringLength(lpcszUri),
		FALSE
	);

	HashTableGetValueEx(
		lphsServer->hhtUris,
		ualKey,
		NULLPTR,
		NULLPTR,
		(LPULONGLONG)&lphmtData
	);

	if (NULLPTR == lphmtData)
	{
		lphmtData = GlobalAllocAndZero(sizeof(HTTP_METHOD_TABLE));
		if (NULLPTR == lphmtData)
		{ 
			ReleaseSingleObject(lphsServer->hLock);
			return FALSE; 
		}

		__AssignCallBack(
			lphmtData,
			hmType,
			lpfnCallBack,
			lpUserData
		);

		HashTableInsertEx(
			lphsServer->hhtUris,
			(ULONGLONG)lpcszUri,
			StringLength(lpcszUri),
			FALSE,
			(ULONGLONG)lphmtData
		);
	}
	else
	{
		__AssignCallBack(
			lphmtData,
			hmType,
			lpfnCallBack,
			lpUserData
		);
	}

	ReleaseSingleObject(lphsServer->hLock);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
LPFN_HTTP_URI_INVOCATION_PROC
__GetCallBack(
	_In_ 		LPHTTP_METHOD_TABLE		lphmtData,
	_In_ 		HTTP_METHOD 			hmType,
	_Out_		DLPVOID 			dlpUserData
){
	EXIT_IF_UNLIKELY_NULL(lphmtData, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(dlpUserData, NULLPTR);
	
	LPFN_HTTP_URI_INVOCATION_PROC lpfnCallBack;
	lpfnCallBack = NULLPTR;

	switch(hmType)
	{
		case HTTP_METHOD_GET:
		{
			lpfnCallBack = lphmtData->lpfnGet;
			*dlpUserData = lphmtData->lpGetData;
			break;
		}

		case HTTP_METHOD_POST:
		{
			lpfnCallBack = lphmtData->lpfnPost;
			*dlpUserData = lphmtData->lpPostData;
			break;
		}

		case HTTP_METHOD_PUT:
		{
			lpfnCallBack = lphmtData->lpfnPut;
			*dlpUserData = lphmtData->lpPutData;
			break;
		}

		case HTTP_METHOD_DELETE:
		{
			lpfnCallBack = lphmtData->lpfnDelete;
			*dlpUserData = lphmtData->lpDeleteData;
			break;
		}

		case HTTP_METHOD_OPTIONS:
		{
			lpfnCallBack = lphmtData->lpfnOptions;
			*dlpUserData = lphmtData->lpOptionsData;
			break;
		}

		case HTTP_METHOD_HEAD:
		{
			lpfnCallBack = lphmtData->lpfnHead;
			*dlpUserData = lphmtData->lpHeadData;
			break;
		}

		case HTTP_METHOD_CONNECT:
		{
			lpfnCallBack = lphmtData->lpfnConnect;
			*dlpUserData = lphmtData->lpConnectData;
			break;
		}

		case HTTP_METHOD_TRACE:
		{
			lpfnCallBack = lphmtData->lpfnTrace;
			*dlpUserData = lphmtData->lpTraceData;
			break;
		}

		case HTTP_METHOD_PATCH:
		{
			lpfnCallBack = lphmtData->lpfnPatch;
			*dlpUserData = lphmtData->lpPatchData;
			break;
		}

		case HTTP_METHOD_UNKNOWN:
		{
			WriteFile(GetStandardError(), "Have HTTP_METHOD_UNKNOWN type\n", 0);
			*dlpUserData = NULLPTR;
			break;
		}

		default:
		{
			WriteFile(GetStandardError(), "Have HTTP_METHOD_UNKNOWN type\n", 0);
			*dlpUserData = NULLPTR;
			break;
		}
	}

	return lpfnCallBack;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_)
CROCK_API
LPFN_HTTP_URI_INVOCATION_PROC
GetHttpUriProc(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_Out_Opt_	DLPVOID 		dlpUserData
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszUri, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(dlpUserData, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, NULLPTR);

	LPHTTP_SERVER lphsServer;
	LPHTTP_METHOD_TABLE lphmtData;
	UARCHLONG ualKey;
	UARCHLONG ualCount;
	UARCHLONG ualIndex;
	UARCHLONG ualJndex;
	DLPSTR dlpszSplit;
	CSTRING csBuffer[1024];
	
	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);
	EXIT_IF_UNLIKELY_NULL(lphsServer, NULLPTR);


	ualKey = HashTableGetKey(
		lphsServer->hhtUris,
		(ULONGLONG)lpcszUri,
		StringLength(lpcszUri),
		FALSE
	);

	HashTableGetValueEx(
		lphsServer->hhtUris,
		ualKey,
		NULLPTR,
		NULLPTR,
		(LPULONGLONG)&lphmtData
	);

	if (NULLPTR == lphmtData)
	{
		ualCount = StringSplit(lpcszUri, "/", &dlpszSplit);

		if (NULLPTR == dlpszSplit || 0 == ualCount)
		{ return NULLPTR; }

		/* Loop backwards through URI to find * */
		for (
			ualIndex = ualCount - 1;
			ualIndex > 0 && NULLPTR == lphmtData;
			ualIndex--
		){
			ZeroMemory(csBuffer, sizeof(csBuffer));

			for (
				ualJndex = 0;
				ualJndex <= ualIndex;
				ualJndex++
			){
				if (0 != StringLength(dlpszSplit[ualJndex]))
				{
					StringConcatenateSafe(csBuffer, "/", sizeof(csBuffer));
					StringConcatenateSafe(csBuffer, dlpszSplit[ualJndex], sizeof(csBuffer) - 1);
				}
			}

			StringConcatenateSafe(csBuffer, "/*", sizeof(csBuffer));

			ualKey = HashTableGetKey(
				lphsServer->hhtUris,
				(ULONGLONG)(LPCSTR)csBuffer,
				StringLength(csBuffer),
				FALSE
			);

			HashTableGetValueEx(
				lphsServer->hhtUris,
				ualKey,
				NULLPTR,
				NULLPTR,
				(LPULONGLONG)&lphmtData
			);
			
		}

		/* One last try */
		if (NULLPTR == lphmtData)
		{
			ZeroMemory(csBuffer, sizeof(csBuffer));
			StringCopySafe(csBuffer, "/*", sizeof(csBuffer));
			
			ualKey = HashTableGetKey(
				lphsServer->hhtUris,
				(ULONGLONG)(LPCSTR)csBuffer,
				StringLength(csBuffer),
				FALSE
			);
			
			HashTableGetValueEx(
				lphsServer->hhtUris,
				ualKey,
				NULLPTR,
				NULLPTR,
				(LPULONGLONG)&lphmtData
			);
		}
	}


	return __GetCallBack(
		lphmtData,
		hmType,
		dlpUserData
	);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_)
CROCK_API 
BOOL 
SetHttpServerHooks(
	_In_		HANDLE 				hHttpServer,
	_In_Opt_ 	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPreRequestHook,
	_In_Opt_ 	LPVOID 				lpPreRequestHookParam,
	_In_Opt_ 	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPostRequestHook,
	_In_Opt_ 	LPVOID 				lpPostRequestHookParam
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, FALSE);

	LPHTTP_SERVER lphsServer;
	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);
	EXIT_IF_UNLIKELY_NULL(lphsServer, NULLPTR);

	WaitForSingleObject(lphsServer->hLock, INFINITE_WAIT_TIME);

	lphsServer->lpfnPreRequestHook = lpfnPreRequestHook;
	lphsServer->lpfnPostRequestHook = lpfnPostRequestHook;
	lphsServer->lpPreRequestHookParam = lpPreRequestHookParam;
	lphsServer->lpPostRequestHookParam = lpPostRequestHookParam;

	ReleaseSingleObject(lphsServer->hLock);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CROCK_API 
BOOL 
GetHttpServerHooks(
	_In_ 		HANDLE 				hHttpServer,
	_Out_Opt_ 	DLPFN_HTTP_URI_INVOCATION_PROC	dlpfnPreRequestHook,
	_Out_Opt_ 	DLPVOID 			dlpPreRequestHookParam,
	_Out_Opt_ 	DLPFN_HTTP_URI_INVOCATION_PROC	dlpfnPostRequestHook,
	_Out_Opt_ 	DLPVOID 			dlpPostRequestHookParam
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, FALSE);

	LPHTTP_SERVER lphsServer;
	LPVOID lpTemp;

	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);
	EXIT_IF_UNLIKELY_NULL(lphsServer, NULLPTR);

	if (NULLPTR != dlpfnPreRequestHook)
	{ *dlpfnPreRequestHook = InterlockedAcquire(&(lphsServer->lpfnPreRequestHook)); }

	if (NULLPTR != dlpPreRequestHookParam)
	{ *dlpPreRequestHookParam = InterlockedAcquire(&(lphsServer->lpPreRequestHookParam)); }

	if (NULLPTR != dlpfnPostRequestHook)
	{ *dlpfnPostRequestHook = InterlockedAcquire(&(lphsServer->lpfnPostRequestHook)); }

	if (NULLPTR != dlpPostRequestHookParam)
	{ *dlpPostRequestHookParam = InterlockedAcquire(&(lphsServer->lpPostRequestHookParam)); }

	return TRUE;
}




#ifdef BUILD_WITH_ONEAGENT_SDK
_Success_(return != ONESDK_INVALID_HANDLE, _Interlocked_Operation_)
CROCK_API
onesdk_webapplicationinfo_handle_t
GetHttpServerWebAppHandle(
	_In_ 		HANDLE 			hHttpServer
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, ONESDK_INVALID_HANDLE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, ONESDK_INVALID_HANDLE);

	LPHTTP_SERVER lphsServer;
	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);
	EXIT_IF_UNLIKELY_NULL(lphsServer, ONESDK_INVALID_HANDLE);

	return lphsServer->owihOneWebApp;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
CROCK_API
HSTRING
GetRumScriptTag(
	_In_ 		HANDLE 			hHttpServer
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, ONESDK_INVALID_HANDLE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, ONESDK_INVALID_HANDLE);

	LPHTTP_SERVER lphsServer;
	HSTRING hsResponse;

	lphsServer = OBJECT_CAST(hHttpServer, LPHTTP_SERVER);
	EXIT_IF_UNLIKELY_NULL(lphsServer, ONESDK_INVALID_HANDLE);

	WaitForSingleObject(lphsServer->hLock, INFINITE_WAIT_TIME);
	hsResponse = CreateHeapString(lphsServer->lpszRumScript);
	ReleaseSingleObject(lphsServer->hLock);

	return hsResponse;
}
#endif