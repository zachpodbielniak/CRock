/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "StaticContent.h"
#include "BuiltInHandlers.h"
#include "magic.h"




_Success_(return != NULLPTR, _Non_Locking_)
CROCK_API 
LPSTATIC_OPTIONS
CreateStaticContentOptions(
	_In_Z_		LPCSTR 			lpcszDirectory,
	_In_Z_		LPCSTR			lpcszBaseUriPath
){
	LPSTATIC_OPTIONS lpsoOptions; 

	lpsoOptions = GlobalAllocAndZero(sizeof(STATIC_OPTIONS));
	if (NULLPTR == lpsoOptions)
	{ return NULLPTR; }

	if (NULLPTR == lpcszDirectory)
	{ 
		lpsoOptions->lpszDirectory = GlobalAllocAndZero(PATH_MAX); 
		getcwd(lpsoOptions->lpszDirectory, PATH_MAX - 1);
	}
	else 
	{ lpsoOptions->lpszDirectory = StringDuplicate(lpcszDirectory); }

	lpsoOptions->lpszBaseUriPath = StringDuplicate(lpcszBaseUriPath);

	return lpsoOptions;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
DestroyStaticContentOptions(
	_In_		LPSTATIC_OPTIONS	lpsoOptions
){
	EXIT_IF_UNLIKELY_NULL(lpsoOptions, FALSE);

	if (NULLPTR != lpsoOptions->lpszDirectory)
	{ FreeMemory(lpsoOptions->lpszDirectory); }

	if (NULLPTR != lpsoOptions->lpszBaseUriPath)
	{ FreeMemory(lpsoOptions->lpszBaseUriPath); }

	FreeMemory(lpsoOptions);
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
INTERNAL_OPERATION
HANDLE
__IfIsSafeToOpenActuallyDoSo(
	_In_Z_		LPCSTR			lpcszPath,
	_In_Z_		LPCSTR			lpcszFile,
	_In_		LPSTATIC_OPTIONS	lpsoOptions,
	_Out_		DLPSTR			dlpszMimeType
){
	EXIT_IF_UNLIKELY_NULL(lpcszPath, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszFile, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpsoOptions, FALSE);
	EXIT_IF_UNLIKELY_NULL(dlpszMimeType, FALSE);
	CSTRING csFilePath[PATH_MAX];
	HANDLE hFile;
	ARCHLONG alIndex;
	ARCHLONG alType;
	magic_t magicCookie;
	LPCSTR lpcszMimeType;

	ZeroMemory(csFilePath, sizeof(csFilePath));
	hFile = NULL_OBJECT;

	/* Attempts to go up a dir = NO! */
	if (FALSE != StringContains(lpcszPath, ".."))
	{ return NULL_OBJECT; }

	if (FALSE != StringContains(lpcszFile, ".."))
	{ return NULL_OBJECT; }

	if (FALSE == StringEndsWith(lpcszPath, "/"))
	{
		StringPrintFormatSafe(
			csFilePath,
			sizeof(csFilePath) - 1,
			"%s/%s",
			lpcszPath,
			lpcszFile
		);
	}
	else 
	{
		StringPrintFormatSafe(
			csFilePath,
			sizeof(csFilePath) - 1,
			"%s%s",
			lpcszPath,
			lpcszFile
		);
	}

	/* Do the magic */
	magicCookie = magic_open(MAGIC_MIME);
	magic_load(magicCookie, NULLPTR);
	lpcszMimeType = magic_file(magicCookie, csFilePath);

	*dlpszMimeType = StringDuplicate(lpcszMimeType);
	magic_close(magicCookie);

	hFile = OpenFile(csFilePath, FILE_PERMISSION_READ, 0);
	return hFile;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GenerateDirectoryListing(
	_In_Z_		LPCSTR			lpcszBaseUri,
	_In_Z_		LPCSTR			lpcszDir,
	_In_Z_		LPCSTR			lpcszPath,
	_In_		LPSTATIC_OPTIONS	lpsoOptions,
	_In_		LPREQUEST_DATA		lprdData
){
	HANDLE hDirectory;
	HVECTOR hvDir;
	CSTRING csPath[PATH_MAX];
	CSTRING csLine[0x400];
	ARCHLONG alIndex;

	ZeroMemory(csPath, sizeof(csPath));

	StringPrintFormatSafe(
		csPath,
		sizeof(csPath) - 1,
		"%s",
		lpcszPath
	);

	if (StringContains(csPath, ".."))
	{ return FALSE; }

	hDirectory = OpenDirectory(lpcszDir);

	if (NULL_OBJECT == hDirectory)
	{ return FALSE; }

	hvDir = DirectoryToVectorListing(hDirectory);

	if (NULL_OBJECT == hvDir)
	{
		DestroyObject(hDirectory);
		return FALSE;
	}

	for (
		alIndex = 0;
		alIndex < (ARCHLONG)VectorSize(hvDir);
		alIndex++
	){
		LPDIRECTORY_ENTRY lpdeEntry;

		ZeroMemory(csLine, sizeof(csLine));
		lpdeEntry = VectorAt(hvDir, alIndex);

		if (NULLPTR == lpdeEntry)
		{ continue; }

		if (DT_REG == lpdeEntry->bType)
		{
			StringPrintFormatSafe(
				csLine,
				sizeof(csLine) - 1,
				"F - <a href='%s%s/%s'>%s</a><br />",
				lpsoOptions->lpszBaseUriPath,
				lpcszPath,
				lpdeEntry->lpszFileName,
				lpdeEntry->lpszFileName
			);
		}
		else if (DT_DIR == lpdeEntry->bType)
		{
			StringPrintFormatSafe(
				csLine,
				sizeof(csLine) - 1,
				"D - <a href='?File=%s/%s&Dir=1'>%s</a><br />",
				lpcszPath,
				lpdeEntry->lpszFileName,
				lpdeEntry->lpszFileName
			);
		}

		RequestAddResponse(lprdData->hRequest, csLine, StringLength(csLine));
	}

	DestroyObject(hDirectory);
}




_Call_Back_
HTTP_STATUS_CODE
StaticContentServer(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	CSTRING csContentType[0x200];
	LPQUERYSTRING_DATA lpqsdInfo;
	LPSTATIC_OPTIONS lpsoOptions;
	HANDLE hFile;
	LPSTR lpszFile;
	LPSTR lpszMimeType;
	LPVOID lpData;
	UARCHLONG ualSize;
	BOOL bDoListing;

	ZeroMemory(csContentType, sizeof(csContentType));

	if (NULLPTR == lpUserData)
	{ 
		return BuiltIn400(
			hHttpServer,
			lpcszUri,
			hmType,
			(LPVOID)"StaticContentServer() Has No Configuration!",
			lprdData
		);
	}

	lpsoOptions = lpUserData;
	bDoListing = FALSE;

	/* Do it based off of file */
	if ((NULLPTR != lprdData->lpszRequestParams && '\0' != *(lprdData->lpszRequestParams)) || NULLPTR == lpsoOptions->lpszBaseUriPath)
	{
		ARCHLONG alIndex;
		
		ProcessQueryStringParameters(lprdData->hRequest);
		UrlDecode(lprdData->hRequest);

		for (
			alIndex = 0;
			alIndex < (ARCHLONG)VectorSize(lprdData->hvQueryParameters);
			alIndex++
		){
			lpqsdInfo = VectorAt(lprdData->hvQueryParameters, alIndex);

			if (0 == StringCompare("File", lpqsdInfo->lpszKey))
			{ lpszFile = lpqsdInfo->lpszValue;}

			else if (0 == StringCompare("Dir", lpqsdInfo->lpszKey))
			{
				if (
					0 == StringCompare("1", lpqsdInfo->lpszValue) ||
					0 == StringCompare("True", lpqsdInfo->lpszValue)
				){
					if (TRUE == lpsoOptions->bListDirectoryContents)
					{ bDoListing = TRUE; }
				}
				
			}
		}

		if (NULLPTR == lpszFile)
		{ 
			return BuiltIn400(
				hHttpServer,
				lpcszUri,
				hmType,
				(LPVOID)"StaticContentServer() Has No ?File= Parameter",
				lprdData
			);
		}

		if (FALSE == bDoListing)
		{
			hFile = __IfIsSafeToOpenActuallyDoSo(
				lpsoOptions->lpszDirectory,
				lpszFile,
				lpsoOptions,
				&lpszMimeType
			);
		}
	}
	else /* Must have lpszBaseUriPath set! */
	{
		ARCHLONG alBaseLength;

		alBaseLength = StringLength(lpsoOptions->lpszBaseUriPath);

		/* Safety net check */
		if (alBaseLength >= (ARCHLONG)StringLength(lpcszUri))
		{
			return BuiltIn400(
				hHttpServer,
				lpcszUri,
				hmType,
				(LPVOID)"The Base URI Is Bigger Than The Requests!",
				lprdData
			);
		}
		
		if (FALSE == bDoListing)
		{
			lpszFile = (LPSTR)(LPVOID)((UARCHLONG)lpcszUri + alBaseLength);
			hFile = __IfIsSafeToOpenActuallyDoSo(
				(LPCSTR)lpsoOptions->lpszDirectory,
				(LPCSTR)lpszFile, 
				lpsoOptions,
				&lpszMimeType
			);
		}
	}


	/* Do File */
	if (FALSE == bDoListing)
	{
		if (NULL_OBJECT == hFile)
		{
			return BuiltIn500(
				hHttpServer,
				lpcszUri,
				hmType,
				(LPVOID)"File could not be opened!",
				lprdData
			);
		}

		ualSize = GetFileSize(hFile);
		lpData = GlobalAllocAndZero(ualSize);

		ReadWholeFile(hFile, (DLPSTR)&lpData, 0);


		/* Set Content Type */
		StringPrintFormatSafe(
			csContentType,
			sizeof(csContentType) - 1,
			"Content-Type: %s",
			lpszMimeType
		);
	

		RequestAddHeader(lprdData->hRequest, csContentType);
		RequestAddResponse(lprdData->hRequest, lpData, ualSize);

		if (NULLPTR != lpszMimeType)
		{ FreeMemory(lpszMimeType); }
	
		FreeMemory(lpData);
		DestroyObject(hFile); 
	}
	else 
	{
		LPSTR lpszHtmlStart = "<html><head><title>CRock -- Dir Listing</title></head><body>";
		LPSTR lpszHtmlEnd = "</body></html>";
		
		CSTRING csPath[PATH_MAX];
		ZeroMemory(csPath, sizeof(csPath));

		StringPrintFormatSafe(
			csPath,
			sizeof(csPath) - 1,
			"%s/%s",
			lpsoOptions->lpszDirectory,
			lpszFile
		);

		RequestAddResponse(lprdData->hRequest, lpszHtmlStart, StringLength(lpszHtmlStart));
		__GenerateDirectoryListing(lpcszUri, csPath, lpszFile, lpsoOptions, lprdData);
		RequestAddResponse(lprdData->hRequest, lpszHtmlEnd, StringLength(lpszHtmlEnd));
		RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	}
	
	return HTTP_STATUS_200_OK;
}


