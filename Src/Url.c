/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/




#include "Url.h"
#include "RequestObject.c"




_Success_(return != FALSE, _Acquires_Shared_Lock_)
CROCK_API 
BOOL
UrlDecode(
	_In_		HANDLE			hRequest
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;
	LPSTR lpszIndex;
	ARCHLONG alIndex;
	ARCHLONG alLength;
	CSTRING csBuffer[4096];
	CSTRING csCharCode[3];
	CSTRING csCat[2];
	CHAR cValue;


	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csCharCode, sizeof(csCharCode));
	ZeroMemory(csCat, sizeof(csCat));

	alLength = StringLength(lpRequest->rdInfo.lpszRequestParams);

	for (
		lpszIndex = lpRequest->rdInfo.lpszRequestParams;
		alLength > alIndex || 0 != *lpszIndex;
		alIndex++, (lpszIndex = (LPSTR)(LPVOID)((UARCHLONG)lpszIndex + 0x01U))
	){
		ZeroMemory(csCat, sizeof(csCat));

		if ('%' == *lpszIndex)
		{
			ZeroMemory(csCharCode, sizeof(csCharCode));

			csCharCode[0] = lpszIndex[1];
			csCharCode[1] = lpszIndex[2];

			StringScanFormat(
				csCharCode,
				"%hhx",
				&cValue
			);

			lpszIndex = (LPSTR)(LPVOID)((UARCHLONG)lpszIndex + 0x02U);
		}
		else 
		{ cValue = *lpszIndex; }

		csCat[0] = cValue;

		StringConcatenateSafe(csBuffer, csCat, sizeof(csBuffer) - 1);
	}

	lpRequest->rdInfo.lpszRequestParamsDecoded = StringDuplicate((LPCSTR)csBuffer);
	if (NULLPTR == lpRequest->rdInfo.lpszRequestParamsDecoded)
	{ return FALSE; }

	return TRUE;
}



