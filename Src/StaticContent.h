/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_STATICCONTENT_H
#define CROCK_STATICCONTENT_H


#include "Prereqs.h"
#include "Connection.h"
#include "Request.h"
#include "HttpStatusCodes.h"
#include "Url.h"




typedef struct __STATIC_OPTIONS
{
	LPSTR 		lpszDirectory;
	LPSTR		lpszBaseUriPath;
	BOOL		bListDirectoryContents;
} STATIC_OPTIONS, *LPSTATIC_OPTIONS;




_Success_(return != NULLPTR, _Non_Locking_)
CROCK_API 
LPSTATIC_OPTIONS
CreateStaticContentOptions(
	_In_Z_		LPCSTR 			lpcszDirectory,
	_In_Z_		LPCSTR			lpcszBaseUriPath
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
DestroyStaticContentOptions(
	_In_		LPSTATIC_OPTIONS	lpsoOptions
);




_Call_Back_
HTTP_STATUS_CODE
StaticContentServer(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
);


#endif
