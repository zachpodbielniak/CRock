/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_DEFS_H
#define CROCK_DEFS_H


#include "Prereqs.h"
#include "TypeDefs.h"



#define CROCK_API				EXTERN
#define CROCK_MODULE				EXTERN


#define HANDLE_TYPE_HTTP_SERVER			0x5c00
#define HANDLE_TYPE_HTTP_SERVER_REQUEST		0x5c01


#define CROCK_SERVER_DEFAULT_URI_HASH_SIZE	16384
#define CROCK_SERVER_DEFAULT_REQUEST_REGION	2048
#define CROCK_SERVER_DEFAULT_RESPONSE_REGION	16384

#endif