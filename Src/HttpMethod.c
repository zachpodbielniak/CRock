/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_HTTP_METHOD_C
#define CROCK_HTTP_METHOD_C


#include "Server.h"


typedef struct __HTTP_METHOD_TABLE
{
	HANDLE 			hHttpServer;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnGet;
	LPVOID 				lpGetData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnHead;
	LPVOID 				lpHeadData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPost;
	LPVOID 				lpPostData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPut;
	LPVOID 				lpPutData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnDelete;
	LPVOID 				lpDeleteData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnConnect;
	LPVOID 				lpConnectData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnOptions;
	LPVOID 				lpOptionsData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnTrace;
	LPVOID 				lpTraceData;

	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPatch;
	LPVOID 				lpPatchData;
} HTTP_METHOD_TABLE, *LPHTTP_METHOD_TABLE, **DLPHTTP_METHOD_TABLE;


#endif