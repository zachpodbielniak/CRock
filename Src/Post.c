/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Post.h"
#include "RequestObject.c"




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL
PostIsMultipartForm(
	_In_ 		HANDLE 		hRequest
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;
	LPSTR lpszBoundary;
	BOOL bRet;

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_LIKELY_NULL(lpRequest, FALSE);

	bRet = FALSE;

	if (NULLPTR == lpRequest->rdInfo.lpszContentType)
	{ return bRet; }

	if (NULLPTR != (lpszBoundary = StringInString(lpRequest->rdInfo.lpszContentType, "boundary=")))
	{ bRet = TRUE; }

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL
ProcessMultipartForm(
	_In_ 		HANDLE 		hRequest
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;
	LPSTR lpszPayload;
	LPSTR lpszBoundary;
	LPSTR lpszIndex;
	LPSTR lpszStart;
	LPSTR lpszEnd;
	LPSTR lpszKey;
	LPSTR lpszValue;
	DLPSTR dlpszBoundary;
	UARCHLONG ualCount;
	UARCHLONG ualBoundaryLength;
	FORM_DATA fdInfo;
	BOOL bRet;
	CSTRING csKey[8192];
	CSTRING csValue[8192];

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_LIKELY_NULL(lpRequest, FALSE);

	bRet = FALSE;

	if (NULLPTR == lpRequest->rdInfo.lpszContentType)
	{ return bRet; }

	if (NULLPTR != (lpszBoundary = StringInString(lpRequest->rdInfo.lpszContentType, "boundary=")))
	{ bRet = TRUE; }

	if (FALSE == bRet)
	{ return bRet; }

	/* Boundary stored in dlpszBoundary[1] */
	ualCount = StringSplit(lpszBoundary, "=", &dlpszBoundary);

	if (2 != ualCount)
	{
		DestroySplitString(dlpszBoundary);
		return FALSE;
	}

	ualBoundaryLength = StringLength(dlpszBoundary[1]);

	lpszPayload = LocalAllocAndZero(lpRequest->rdInfo.ualContentLength + 1);

	CopyMemory(lpszPayload, lpRequest->rdInfo.lpRequestData, lpRequest->rdInfo.ualContentLength);

	/* Find the next lpszBoundary, and parse out the key, and value. */

	/* 
		Format:
		--------------------dlpszBoundary\r\n
		Content-Disposition: form-data; name="key1"\r\n
		\r\n
		value1\r\n
		--------------------dlpszBoundary\r\n
		Content-Disposition: form-data; name="key2"\r\n
		\r\n
		value2\r\n
		--------------------dlpszBoundary--
	*/

	/* The "--" after the dlpszBoundary on the last one signifies the end. */

	ualCount = lpRequest->rdInfo.ualContentLength;
	for (
		lpszStart = StringInString(lpszPayload, dlpszBoundary[1]);
		ualCount != 0;
		ualCount--, lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U)
	){
		ZeroMemory(&fdInfo, sizeof(FORM_DATA));
		ZeroMemory(csKey, sizeof(csKey));
		ZeroMemory(csValue, sizeof(csValue));

		/* Add The Offset Of The Boundary */
		lpszIndex = StringInString(lpszStart, dlpszBoundary[1]);
		if (NULLPTR == lpszIndex)
		{ JUMP(__DONE); }

		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + ualBoundaryLength);
		ualCount -= ualBoundaryLength;

		/* Now Check To See If Its The End '--' */
		if ('-' == *lpszIndex)
		{ JUMP(__DONE); }

		/* Add Offset Of \r\n */
		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x02U);
		ualCount -= 0x02U;

		/* Find The '=' */
		while ('=' != *lpszIndex && '\0' != *lpszIndex)
		{ 
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);
			ualCount -= 0x01U;
		}

		/* Account for "=\""" */
		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x02U);
		ualCount -= 0x02U;

		/* Set Start Of Copy */
		lpszStart = lpszIndex;
		while ('\"' != *lpszIndex && '\0' != *lpszIndex)
		{ 
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);
			ualCount -= 0x01U;
		}

		/* Copy Out Key */
		CopyMemory(
			csKey,
			lpszStart,
			((UARCHLONG)lpszIndex - (UARCHLONG)lpszStart)
		);
		
		/* Add 5 To Account For \"\r\n\r\n */
		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x05U);
		ualCount -= 0x05U;

		/* End Will Be Next \r\n */
		lpszEnd = StringInString(lpszIndex, "\r\n");

		CopyMemory(
			csValue,
			lpszIndex,
			(UARCHLONG)lpszEnd - (UARCHLONG)lpszIndex
		);

		lpszKey = GlobalAllocAndZero(StringLength(csKey) + 1);
		lpszValue = GlobalAllocAndZero(StringLength(csValue) + 1);

		CopyMemory(lpszKey, csKey, StringLength(csKey));
		CopyMemory(lpszValue, csValue, StringLength(csValue));

		fdInfo.lpszKey = lpszKey;
		fdInfo.lpszValue = lpszValue;

		if (NULLPTR == lpRequest->rdInfo.hvFormData)
		{ lpRequest->rdInfo.hvFormData = CreateVector(8, sizeof(FORM_DATA), 0); }

		VectorPushBack(lpRequest->rdInfo.hvFormData, &fdInfo, 0);

		/* Add Offset */
		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x04U);
		ualCount -= 0x04U;
	}
	__DONE:

	DestroySplitString(dlpszBoundary);
	FreeMemory(lpszPayload);
	return bRet;
}