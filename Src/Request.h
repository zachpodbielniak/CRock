/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_REQUEST_H
#define CROCK_REQUEST_H


#include "Prereqs.h"
#include "Connection.h"
#include "HttpStatusCodes.h"




typedef enum __HTTP_METHOD
{
	HTTP_METHOD_UNKNOWN	= 0x00,
	HTTP_METHOD_GET		= 0x01<<0,
	HTTP_METHOD_HEAD	= 0x01<<1,
	HTTP_METHOD_POST 	= 0x01<<2,
	HTTP_METHOD_PUT		= 0x01<<3,
	HTTP_METHOD_DELETE	= 0x01<<4,
	HTTP_METHOD_CONNECT	= 0x01<<5,
	HTTP_METHOD_OPTIONS	= 0x01<<6,
	HTTP_METHOD_TRACE	= 0x01<<7,
	HTTP_METHOD_PATCH	= 0x01<<8
} HTTP_METHOD;




#define HTTP_METHOD_TO_STRING(X, O)		\
	if (HTTP_METHOD_GET == (X))		\
	{ (O) = "GET"; }			\
	else if (HTTP_METHOD_HEAD == (X))	\
	{ (O) = "HEAD"; }			\
	else if (HTTP_METHOD_POST == (X))	\
	{ (O) = "POST"; }			\
	else if (HTTP_METHOD_PUT == (X))	\
	{ (O) = "PUT"; }			\
	else if (HTTP_METHOD_DELETE == (X))	\
	{ (O) = "DELETE"; }			\
	else if (HTTP_METHOD_CONNECT == (X))	\
	{ (O) = "CONNECT"; }			\
	else if (HTTP_METHOD_OPTIONS == (X))	\
	{ (O) = "OPTIONS"; }			\
	else if (HTTP_METHOD_TRACE == (X))	\
	{ (O) = "TRACE"; }			\
	else if (HTTP_METHOD_PATCH == (X))	\
	{ (O) = "PATCH"; }			\
	else 					\
	{ (O) = "UNKNOWN"; }




typedef struct __REQUEST_DATA
{
	HANDLE 			hHttpServer;
	HANDLE 			hRequest;
	HANDLE 			hClient;

	HTTP_METHOD		hmType;
	HTTP_VERSION		htvType;
	UARCHLONG 		ualContentLength;

	LPSTR 			lpszRequestUri;
	LPSTR 			lpszRequestParams;
	LPSTR			lpszRequestParamsDecoded;
	LPSTR 			lpszRequestConnection;

	LPSTR 			lpszAccept;
	LPSTR 			lpszContentType;
	LPSTR 			lpszHost;
	LPSTR 			lpszUserAgent;
	LPSTR 			lpszXRealIp;
	LPSTR 			lpszXForwardedFor;

	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR 			lpszXDynatrace;
	#endif

	LPVOID 			lpRequestData;
	LPVOID 			lpRequestBody;
	HVECTOR			hvRequestHeaders;
	HVECTOR 		hvQueryParameters;
	HVECTOR 		hvFormData;
} REQUEST_DATA, *LPREQUEST_DATA, **DLPREQUEST_DATA;




typedef struct __REQUEST_HEADER
{
	LPSTR 			lpszHeader;
	LPSTR 			lpszValue;
} REQUEST_HEADER, *LPREQUEST_HEADER;




typedef struct __FORM_DATA
{
	LPSTR 			lpszKey;
	LPSTR 			lpszValue;
} FORM_DATA, *LPFORM_DATA;




typedef struct __QUERYSTRING_DATA
{
	LPSTR 			lpszKey;
	LPSTR 			lpszValue;
} QUERYSTRING_DATA, *LPQUERYSTRING_DATA;




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateRequest(
	_In_ 		HANDLE 		hHttpServer,
	_In_ 		HANDLE 		hClient,
	_In_ 		HHASHTABLE	hhtUris,
	_In_ 		HANDLE 		hRequestRegion
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
HandleRequest(
	_In_ 		HANDLE 		hRequest
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
RequestAddHeader(
	_In_ 		HANDLE 		hRequest,
	_In_ 		LPCSTR 		lpcszHeader
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
RequestAddResponse(
	_In_ 		HANDLE 		hRequest,
	_In_ 		LPVOID 		lpData,
	_In_		UARCHLONG 	ualSize
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
ProcessQueryStringParameters(
	_In_ 		HANDLE 		hRequest
);







#endif