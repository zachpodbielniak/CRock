/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Defs.h"
#include "Request.h"

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif


/*
VOID 
OnLoad(
	VOID
) __attribute__ ((constructor));




VOID 
OnDestroy(
	VOID
) __attribute__ ((destructor));




VOID 
OnLoad(
	VOID
){

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_result_t orInit;

	orInit = onesdk_initialize_2(0);
	if (ONESDK_SUCCESS != orInit)
	{ PostQuitMessage(254); }
	#endif
}



VOID 
OnDestroy(
	VOID
){
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_shutdown();
	#endif
}
*/