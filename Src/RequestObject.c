/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_REQUESTOBJECT_C
#define CROCK_REQUESTOBJECT_C


#include "Defs.h"
#include "Request.h"

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif



#ifndef CROCK_REQUEST_DEFINED
#define CROCK_REQUEST_DEFINED
typedef struct __REQUEST
{
	INHERITS_FROM_HANDLE();	
	HANDLE 			hRequestPayload;
	HANDLE 			hResponsePayload;
	HANDLE 			hTempHeaderPayload;
	HANDLE 			hTempResponsePayload;	
	REQUEST_DATA 		rdInfo;
	LPSTR 			lpszHttpMethod;

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_handle_t	othTracer;
	#endif
} REQUEST, *LPREQUEST;
#endif



#endif