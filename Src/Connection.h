/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_CONNECTION_H
#define CROCK_CONNECTION_H


#include "Defs.h"




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateConnection(
	_In_ 		HANDLE 			hClientSocket
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
BOOL 
ConnectionRead(
	_In_ 		HANDLE RESTRICT 	hConnection,
	_In_ 		HANDLE RESTRICT 	hDynamicRegion
);




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
ConnectionWrite(
	_In_ 		HANDLE RESTRICT 	hConnection,
	_In_ 		HANDLE RESTRICT 	hDynamicRegion
);




#endif