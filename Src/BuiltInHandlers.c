/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "BuiltInHandlers.h"




_Call_Back_
HTTP_STATUS_CODE
BuiltIn400(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(hmType);

	CSTRING csResponse[8192];
	CSTRING csTemp[1024];

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csTemp, sizeof(csTemp));

	if (NULLPTR != lpUserData)
	{
		LPSTR lpszMessage;
		lpszMessage = lpUserData;

		StringConcatenateSafe(
			csResponse,
			"<html><head><title>CRock -- 400 Bad Request</title></head><body>",
			sizeof(csResponse) - 1
		);
	
		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>404 Not Found</h1>"
			"<div>%s</div>"
			"<div>%s</div>"
			"</body></html>",
			lpcszUri,
			lpszMessage
		);
	}
	else
	{
		StringConcatenateSafe(
			csResponse,
			"<html><head><title>CRock -- 400 Bad Request</title></head><body>",
			sizeof(csResponse) - 1
		);

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>400 Bad Request</h1>"
			"<div>%s<div>"
			"</body></html>",
			lpcszUri
		);
	}


	StringConcatenateSafe(
		csResponse,
		csTemp,
		sizeof(csResponse) - 1
	);


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	RequestAddHeader(lprdData->hRequest, "X-Response-Code: 400");
	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));

	return HTTP_STATUS_400_BAD_REQUEST;
}




_Call_Back_
HTTP_STATUS_CODE
BuiltIn404(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	CSTRING csResponse[8192];
	CSTRING csTemp[1024];

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csTemp, sizeof(csTemp));

	StringConcatenateSafe(
		csResponse,
		"<html><head><title>CRock -- 404 Not Found</title></head><body>",
		sizeof(csResponse) - 1
	);

	StringPrintFormatSafe(
		csTemp,
		sizeof(csTemp) - 1,
		"<h1>404 Not Found</h1>"
		"%s Is Not A Valid URI"
		"</body></html>",
		lpcszUri
	);

	StringConcatenateSafe(
		csResponse,
		csTemp,
		sizeof(csResponse) - 1
	);


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	RequestAddHeader(lprdData->hRequest, "X-Response-Code: 404");
	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));

	return HTTP_STATUS_404_NOT_FOUND;
}




_Call_Back_
HTTP_STATUS_CODE
BuiltIn500(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(hmType);

	CSTRING csResponse[8192];
	CSTRING csTemp[1024];

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csTemp, sizeof(csTemp));

	if (NULLPTR != lpUserData)
	{
		LPSTR lpszMessage;
		lpszMessage = lpUserData;

		StringConcatenateSafe(
			csResponse,
			"<html><head><title>CRock -- 500 Internal Server Error</title></head><body>",
			sizeof(csResponse) - 1
		);
	
		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>500 Internal Server Error</h1>"
			"<div>%s</div>"
			"<div>%s</div>"
			"</body></html>",
			lpcszUri,
			lpszMessage
		);
	}
	else
	{
		StringConcatenateSafe(
			csResponse,
			"<html><head><title>CRock -- 500 Internal Server Error</title></head><body>",
			sizeof(csResponse) - 1
		);

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"<h1>500 Internal Server Error</h1>"
			"<div>%s<div>"
			"</body></html>",
			lpcszUri
		);
	}


	StringConcatenateSafe(
		csResponse,
		csTemp,
		sizeof(csResponse) - 1
	);


	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");
	RequestAddHeader(lprdData->hRequest, "X-Response-Code: 500");
	RequestAddResponse(lprdData->hRequest, csResponse, StringLength(csResponse));

	return HTTP_STATUS_500_INTERNAL_SERVER_ERROR;
}





