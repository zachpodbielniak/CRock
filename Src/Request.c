/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Request.h"
#include "BuiltInHandlers.h"
#include "HttpMethod.c"
#include "RequestObject.c"




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__HeaderHandler(
	_In_ 		LPREQUEST 	lpRequest,
	_In_Z_ 		LPSTR 		lpszHeader
){
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszHeader, FALSE);

	CSTRING csHeader[1152];
	CSTRING csValue[1024];
	LPSTR lpszIndex;
	UARCHLONG ualIndex;
	BOOL bDetectedHeader;

	ZeroMemory(csHeader, sizeof(csHeader));
	ZeroMemory(csValue, sizeof(csValue));

	StringCopySafe(csHeader, lpszHeader, sizeof(csHeader) - 1);
	lpszIndex = (LPSTR)csHeader;
	bDetectedHeader = FALSE;


	/* Split Field And Value */
	while ('\0' != *lpszIndex)
	{
		if (':' == *lpszIndex)
		{
			*lpszIndex = '\0';
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

			/* If Its A Space, Add */
			while (' ' == *lpszIndex)
			{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }
			
			bDetectedHeader = TRUE;
			JUMP(__DONE_SPLITTING);
		}
		lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);
	}
	__DONE_SPLITTING:

	if (FALSE != bDetectedHeader)
	{ StringCopySafe(csValue, lpszIndex, sizeof(csValue) - 1); }
	else 
	{ return FALSE; }

	for (
		ualIndex = 0;
		ualIndex < StringLength(csHeader);
		ualIndex++
	){ csHeader[ualIndex] = tolower(csHeader[ualIndex]); }

	if (0 == StringCompare("host", csHeader))
	{
		lpRequest->rdInfo.lpszHost = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszHost, csValue);
	}
	else if (0 == StringCompare("accept", csHeader))
	{
		lpRequest->rdInfo.lpszAccept = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszAccept, csValue);
	}
	else if (0 == StringCompare("content-type", csHeader))
	{
		lpRequest->rdInfo.lpszContentType = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszContentType, csValue);
	}
	else if (0 == StringCompare("content-length", csHeader))
	{
		StringScanFormat(csValue, "%lu", &(lpRequest->rdInfo.ualContentLength));
	}
	else if (0 == StringCompare("user-agent", csHeader))
	{
		lpRequest->rdInfo.lpszUserAgent = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszUserAgent, csValue);
	}
	else if (0 == StringCompare("x-real-ip", csHeader))
	{
		lpRequest->rdInfo.lpszXRealIp = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszXRealIp, csValue);
	}
	else if (0 == StringCompare("x-forwarded-for", csHeader))
	{
		lpRequest->rdInfo.lpszXForwardedFor = GlobalAllocAndZero(StringLength(csValue) + 0x01U);
		StringCopy(lpRequest->rdInfo.lpszXForwardedFor, csValue);
	}
	#ifdef BUILD_WITH_ONEAGENT_SDK
	else if (0 == StringCompare("x-dynatrace", csHeader))
	{
		lpRequest->rdInfo.lpszXDynatrace = GlobalAllocAndZero(StringLength(csValue + 0x01U));
		StringCopy(lpRequest->rdInfo.lpszXDynatrace, csValue);
	}
	#endif
	/* Handle Non-Common Headers */
	else
	{
		REQUEST_HEADER rhInfo;
		rhInfo.lpszHeader = GlobalAllocAndZero(StringLength(csHeader) + 0x01U);
		rhInfo.lpszValue = GlobalAllocAndZero(StringLength(csValue) + 0x01U);

		StringCopy(rhInfo.lpszHeader, csHeader);
		StringCopy(rhInfo.lpszValue, csValue);

		VectorPushBack(lpRequest->rdInfo.hvRequestHeaders, &rhInfo, 0);
	}
	
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ExtractUri(
	_In_ 		LPREQUEST	lpRequest,
	_In_ 		LPCSTR 		lpcszData
){
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszData, FALSE);

	DLPSTR dlpszInfo;
	LPSTR lpszParams;
	UARCHLONG ualCount;
	

	ualCount = StringSplit(lpcszData, " ", &dlpszInfo);

	if (3 != ualCount)
	{
		DestroySplitString(dlpszInfo);
		return FALSE;
	}
	

	/* See If There Are Any URI Params */
	lpszParams = dlpszInfo[1];
	while('\0' != *lpszParams)
	{
		if ('?' == *lpszParams)
		{ 
			*lpszParams = '\0';
			lpszParams = (LPSTR)((UARCHLONG)lpszParams + 0x01U);
			JUMP(__MOVE_ON);
		}
		lpszParams = (LPSTR)((UARCHLONG)lpszParams + 0x01U);
	}
	__MOVE_ON:

	/* Copy URI Path */
	if (lpszParams != dlpszInfo[1])
	{ 
		lpRequest->rdInfo.lpszRequestParams = GlobalAllocAndZero(StringLength(lpszParams) + 0x01U); 
		StringCopy(lpRequest->rdInfo.lpszRequestParams, lpszParams);
	}
	lpRequest->rdInfo.lpszRequestUri = GlobalAllocAndZero(StringLength(dlpszInfo[1]) + 0x01U);
	StringCopy(lpRequest->rdInfo.lpszRequestUri, dlpszInfo[1]);

	/* Get HTTP Method */
	if (0 == StringCompare("GET", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_GET; }
	else if (0 == StringCompare("HEAD", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_HEAD; }
	else if (0 == StringCompare("POST", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_POST; }
	else if (0 == StringCompare("PUT", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_PUT; }
	else if (0 == StringCompare("DELETE", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_DELETE; }
	else if (0 == StringCompare("CONNECT", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_CONNECT; }
	else if (0 == StringCompare("OPTIONS", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_OPTIONS; }
	else if (0 == StringCompare("TRACE", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_TRACE; }
	else if (0 == StringCompare("PATCH", dlpszInfo[0]))
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_PATCH; }
	else
	{ lpRequest->rdInfo.hmType = HTTP_METHOD_UNKNOWN; }

	lpRequest->lpszHttpMethod = GlobalAllocAndZero(sizeof(CHAR) * StringLength(dlpszInfo[0]) + 0x01U);
	if (NULLPTR != lpRequest->lpszHttpMethod)
	{ StringCopy(lpRequest->lpszHttpMethod, dlpszInfo[0]); }

	/* Get HTTP Version */
	if (0 == StringCompare("HTTP/1.1", dlpszInfo[2]))
	{ lpRequest->rdInfo.htvType = HTTP_VERSION_1_1; }
	else if (0 == StringCompare("HTTP/2", dlpszInfo[2]))
	{ lpRequest->rdInfo.htvType = HTTP_VERSION_2_0; }
	else if (0 == StringCompare("HTTP/1.0", dlpszInfo[2]))
	{ lpRequest->rdInfo.htvType = HTTP_VERSION_1_0; }
	else 
	{ lpRequest->rdInfo.htvType = HTTP_VERSION_1_0; }

	DestroySplitString(dlpszInfo);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__DestroyRequest(
	_In_ 		HDERIVATIVE 	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPREQUEST lprData;
	UARCHLONG ualIndex;

	lprData = (LPREQUEST)hDerivative;

	if (NULLPTR != lprData->rdInfo.hvRequestHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lprData->rdInfo.hvRequestHeaders);
			ualIndex++
		){
			LPREQUEST_HEADER lprhData;

			lprhData = (LPREQUEST_HEADER)VectorAt(lprData->rdInfo.hvRequestHeaders, ualIndex);
			if (NULLPTR != lprhData)
			{
				if (NULLPTR != lprhData->lpszHeader)
				{ FreeMemory(lprhData->lpszHeader); }

				if (NULLPTR != lprhData->lpszValue) 
				{ FreeMemory(lprhData->lpszValue); }
			}
		}

		DestroyVector(lprData->rdInfo.hvRequestHeaders);
	}
	

	if (NULLPTR != lprData->rdInfo.hvFormData)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lprData->rdInfo.hvFormData);
			ualIndex++
		){
			LPFORM_DATA lpfdData;

			lpfdData = (LPFORM_DATA)VectorAt(lprData->rdInfo.hvFormData, ualIndex);
			if (NULLPTR != lpfdData)
			{
				if (NULLPTR != lpfdData->lpszKey)
				{ FreeMemory(lpfdData->lpszKey); }

				if (NULLPTR != lpfdData->lpszValue)
				{ FreeMemory(lpfdData->lpszValue); }
			}
		}

		DestroyVector(lprData->rdInfo.hvFormData);
	}


	if (NULLPTR != lprData->rdInfo.hvQueryParameters)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lprData->rdInfo.hvQueryParameters);
			ualIndex++
		){
			LPQUERYSTRING_DATA lpqsdData;

			lpqsdData = (LPQUERYSTRING_DATA)VectorAt(lprData->rdInfo.hvQueryParameters, ualIndex);
			if (NULLPTR != lpqsdData)
			{
				if (NULLPTR != lpqsdData->lpszKey)
				{ FreeMemory(lpqsdData->lpszKey); }

				if (NULLPTR != lpqsdData->lpszValue)
				{ FreeMemory(lpqsdData->lpszValue); }
			}
		}
		
		DestroyVector(lprData->rdInfo.hvQueryParameters);
	}

	
	
	if (NULLPTR != lprData->rdInfo.lpszAccept)
	{ FreeMemory(lprData->rdInfo.lpszAccept) ;}
	if (NULLPTR != lprData->rdInfo.lpszHost)
	{ FreeMemory(lprData->rdInfo.lpszHost) ;}
	if (NULLPTR != lprData->rdInfo.lpszContentType)
	{ FreeMemory(lprData->rdInfo.lpszContentType); }
	if (NULLPTR != lprData->rdInfo.lpszRequestConnection)
	{ FreeMemory(lprData->rdInfo.lpszRequestConnection) ;}
	if (NULLPTR != lprData->rdInfo.lpszRequestUri)
	{ FreeMemory(lprData->rdInfo.lpszRequestUri) ;}
	if (NULLPTR != lprData->rdInfo.lpszRequestParams)
	{ FreeMemory(lprData->rdInfo.lpszRequestParams) ;}
	if (NULLPTR != lprData->rdInfo.lpszRequestParamsDecoded)
	{ FreeMemory(lprData->rdInfo.lpszRequestParamsDecoded) ;}
	if (NULLPTR != lprData->rdInfo.lpszUserAgent)
	{ FreeMemory(lprData->rdInfo.lpszUserAgent) ;}
	if (NULLPTR != lprData->rdInfo.lpszXForwardedFor)
	{ FreeMemory(lprData->rdInfo.lpszXForwardedFor) ;}
	if (NULLPTR != lprData->rdInfo.lpszXRealIp)
	{ FreeMemory(lprData->rdInfo.lpszXRealIp) ;}
	if (NULLPTR != lprData->lpszHttpMethod)
	{ FreeMemory(lprData->lpszHttpMethod); }
	
	if (NULL_OBJECT != lprData->hResponsePayload)
	{ DestroyObject(lprData->hResponsePayload); }
	if (NULL_OBJECT != lprData->hRequestPayload) 
	{ DestroyObject(lprData->hRequestPayload); }
	if (NULL_OBJECT != lprData->hTempHeaderPayload)
	{ DestroyObject(lprData->hTempHeaderPayload); }
	if (NULL_OBJECT != lprData->hTempResponsePayload)
	{ DestroyObject(lprData->hTempResponsePayload); }
	FreeMemory(lprData);

	return TRUE;
}





_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateRequest(
	_In_ 		HANDLE 		hHttpServer,
	_In_ 		HANDLE 		hClient,
	_In_ 		HHASHTABLE	hhtUris,
	_In_ 		HANDLE 		hRequestRegion
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(hClient, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(hhtUris, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(hRequestRegion, NULL_OBJECT);

	HANDLE hRequest;
	LPREQUEST lprData;

	lprData = (LPREQUEST)GlobalAllocAndZero(sizeof(REQUEST));
	if (NULLPTR == lprData)
	{ return NULL_OBJECT; }

	lprData->hRequestPayload = hRequestRegion;

	lprData->hResponsePayload = CreateDynamicHeapRegion(NULL_OBJECT, CROCK_SERVER_DEFAULT_RESPONSE_REGION, FALSE, 0);
	if (NULL_OBJECT == lprData->hResponsePayload)
	{
		DestroyObject(lprData->hRequestPayload);
		FreeMemory(lprData);
		return NULL_OBJECT;
	}

	lprData->hTempResponsePayload = CreateDynamicHeapRegion(NULL_OBJECT, CROCK_SERVER_DEFAULT_RESPONSE_REGION, FALSE, 0);
	if (NULL_OBJECT == lprData->hResponsePayload)
	{
		DestroyObject(lprData->hResponsePayload);
		DestroyObject(lprData->hRequestPayload);
		FreeMemory(lprData);
		return NULL_OBJECT;
	}

	lprData->hTempHeaderPayload = CreateDynamicHeapRegion(NULL_OBJECT, CROCK_SERVER_DEFAULT_RESPONSE_REGION, FALSE, 0);
	if (NULL_OBJECT == lprData->hResponsePayload)
	{
		DestroyObject(lprData->hTempResponsePayload);
		DestroyObject(lprData->hResponsePayload);
		DestroyObject(lprData->hRequestPayload);
		FreeMemory(lprData);
		return NULL_OBJECT;
	}

	lprData->rdInfo.hHttpServer = hHttpServer;
	lprData->rdInfo.hClient = hClient;
	lprData->rdInfo.hvRequestHeaders = CreateVector(8, sizeof(REQUEST_HEADER), 0);
	if (NULL_OBJECT == lprData->rdInfo.hvRequestHeaders)
	{
		DestroyObject(lprData->hTempHeaderPayload);
		DestroyObject(lprData->hTempResponsePayload);
		DestroyObject(lprData->hResponsePayload);
		DestroyObject(lprData->hRequestPayload);
		FreeMemory(lprData);
		return NULL_OBJECT;
	}

	hRequest = CreateHandleWithSingleInheritor(
		lprData,
		&(lprData->hThis),
		HANDLE_TYPE_HTTP_SERVER_REQUEST,
		__DestroyRequest,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lprData->ullId),
		0
	);

	if (NULL_OBJECT == hRequest)
	{
		DestroyVector(lprData->rdInfo.hvRequestHeaders);
		DestroyObject(lprData->hTempHeaderPayload);
		DestroyObject(lprData->hTempResponsePayload);
		DestroyObject(lprData->hResponsePayload);
		DestroyObject(lprData->hRequestPayload);
		FreeMemory(lprData);
		return NULL_OBJECT;
	}

	lprData->rdInfo.hRequest = hRequest;
	return hRequest;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
HandleRequest(
	_In_ 		HANDLE 		hRequest
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);
	
	LPREQUEST lpRequest;
	LPSTR lpszPayload;
	CSTRING csTemp[8192];
	LPSTR lpszIndex;
	LPSTR lpszCopy;
	LPVOID lpData;
	LPFN_HTTP_URI_INVOCATION_PROC lpfnCallBack;
	LPFN_HTTP_URI_INVOCATION_PROC lpfnPreRequestHook;
	LPFN_HTTP_URI_INVOCATION_PROC lpfnPostRequestHook;
	LPVOID lpPreRequestHookParam;
	LPVOID lpPostRequestHookParam;
	LPVOID lpUserData;
	LPVOID lpResponse;
	LPVOID lpUserResponse;
	LPVOID lpUserHeader;
	UARCHLONG ualResponseSize;
	UARCHLONG ualUserResponseSize;
	UARCHLONG ualUserHeaderSize;
	UARCHLONG ualSize;
	UARCHLONG ualProcessed;
	HTTP_STATUS_CODE hscRet;
	LPSTR lpszStatusMessage;
	BOOL bGotUri;
	BOOL bAdded;

	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csRequestInfo[1024];
	UARCHLONG ualIndex;
	IPV4_ADDRESS ip4Client;
	LPIPV6_ADDRESS lpip6Client;
	onesdk_stub_version_t ossvInfo;
	#endif

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);

	lpfnCallBack = NULLPTR;
	lpfnPreRequestHook = NULLPTR;
	lpfnPostRequestHook = NULLPTR;
	lpPreRequestHookParam = NULLPTR;
	lpPostRequestHookParam = NULLPTR;
	lpszPayload = (LPSTR)DynamicHeapRegionData(lpRequest->hRequestPayload, &ualSize);
	lpszIndex = lpszPayload;
	lpszCopy = lpszPayload;
	ualProcessed = 0;
	bGotUri = FALSE;
	hscRet = 0;

	/* Parse Out Request */
	__BEGIN_LOOP:
	while ('\0' != *lpszIndex /*&& ualProcessed < ualSize*/)
	{
		bAdded = FALSE;

		/* Check For \r\n */
		if ('\r' == *lpszIndex)
		{
			*lpszIndex = '\0';
			lpszIndex = (LPVOID)((UARCHLONG)lpszIndex + 0x01U);
			*lpszIndex = '\0';
			lpszIndex = (LPVOID)((UARCHLONG)lpszIndex + 0x01U);
			ualProcessed += 2;
			bAdded = TRUE;

			StringCopySafe(csTemp, lpszCopy, sizeof(csTemp) - 1);
			if (FALSE != bGotUri)
			{ __HeaderHandler(lpRequest, csTemp); }
			else 
			{ 
				__ExtractUri(lpRequest, csTemp);
				bGotUri = TRUE;
			}

			/* Check For End Of Header */
			if ('\r' == *lpszIndex)
			{
				lpData = (LPVOID)((UARCHLONG)lpszIndex + 0x02U);
				ualProcessed += 2;
				JUMP(__END_LOOP);
			}

			lpszCopy = lpszIndex;
			JUMP(__BEGIN_LOOP);
		}

		lpszIndex = (LPVOID)((UARCHLONG)lpszIndex + 0x01U);
		if (FALSE == bAdded)
		{ ualProcessed += 2; }
	}
	__END_LOOP:

	lpData = (LPVOID)((UARCHLONG)lpszIndex + 0x02U);
	lpRequest->rdInfo.lpRequestData = lpData;

	lpRequest->rdInfo.lpRequestBody = StringInString(lpData, "\r\n\r\n");
	lpRequest->rdInfo.lpRequestBody = (LPVOID)((UARCHLONG)lpRequest->rdInfo.lpRequestBody + 0x04U);
	

	#ifdef BUILD_WITH_ONEAGENT_SDK
	ZeroMemory(csRequestInfo, sizeof(csRequestInfo));

	StringPrintFormatSafe(
		csRequestInfo,
		sizeof(csRequestInfo) - 1,
		"%s?%s",
		lpRequest->rdInfo.lpszRequestUri,
		lpRequest->rdInfo.lpszRequestParams
	);

	lpRequest->othTracer = onesdk_incomingwebrequesttracer_create(
		GetHttpServerWebAppHandle(lpRequest->rdInfo.hHttpServer),
		onesdk_asciistr(csRequestInfo),
		onesdk_asciistr(lpRequest->lpszHttpMethod)
	);

	if (NULLPTR != lpRequest->rdInfo.lpszHost)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("Host"),
			onesdk_asciistr(lpRequest->rdInfo.lpszHost)
		);
	}
	
	if (NULLPTR != lpRequest->rdInfo.lpszAccept)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("Accept"),
			onesdk_asciistr(lpRequest->rdInfo.lpszAccept)
		);
	}

	if (NULLPTR != lpRequest->rdInfo.lpszContentType)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("Content-Type"),
			onesdk_asciistr(lpRequest->rdInfo.lpszContentType)
		);
	}

	if (NULLPTR != lpRequest->rdInfo.lpszUserAgent)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("User-Agent"),
			onesdk_asciistr(lpRequest->rdInfo.lpszUserAgent)
		);
	}

	if (NULLPTR != lpRequest->rdInfo.lpszXForwardedFor)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("X-Forwarded-For"),
			onesdk_asciistr(lpRequest->rdInfo.lpszXForwardedFor)
		);
	}
	
	if (NULLPTR != lpRequest->rdInfo.lpszXRealIp)
	{
		onesdk_incomingwebrequesttracer_add_request_header(
			lpRequest->othTracer,
			onesdk_asciistr("X-Real-IP"),
			onesdk_asciistr(lpRequest->rdInfo.lpszXRealIp)
		);
	}

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpRequest->rdInfo.hvRequestHeaders);
		ualIndex++
	){
		LPREQUEST_HEADER lprhHeader;

		lprhHeader = VectorAt(lpRequest->rdInfo.hvRequestHeaders, ualIndex);

		if (NULLPTR != lprhHeader)
		{
			onesdk_incomingwebrequesttracer_add_request_header(
				lpRequest->othTracer,
				onesdk_asciistr(lprhHeader->lpszHeader),
				onesdk_asciistr(lprhHeader->lpszValue)
			);
		}
	}

	ZeroMemory(csRequestInfo, sizeof(csRequestInfo));

	if (HANDLE_TYPE_SOCKET6 == GetHandleDerivativeType(lpRequest->rdInfo.hClient))
	{
		lpip6Client = GetSocketIpAddress6(lpRequest->rdInfo.hClient);
		if (NULLPTR != lpip6Client)
		{ ConvertIpv6AddressToCompressedString(lpip6Client, csRequestInfo, sizeof(csRequestInfo) - 1);}
	}
	else 
	{
		ip4Client = GetSocketIpAddress4(lpRequest->rdInfo.hClient);
		ConvertIpv4AddressToString(ip4Client, csRequestInfo, sizeof(csRequestInfo) - 1);
	}
	
	onesdk_incomingwebrequesttracer_set_remote_address(
		lpRequest->othTracer,
		onesdk_asciistr(csRequestInfo)
	);

	onesdk_tracer_start(lpRequest->othTracer);
	#endif

	GetHttpServerHooks(
		lpRequest->rdInfo.hHttpServer,
		&lpfnPreRequestHook,
		&lpPreRequestHookParam,
		&lpfnPostRequestHook,
		&lpPostRequestHookParam
	);

	lpfnCallBack = GetHttpUriProc(
		lpRequest->rdInfo.hHttpServer,
		lpRequest->rdInfo.lpszRequestUri,
		lpRequest->rdInfo.hmType,
		&lpUserData
	);

	if (NULLPTR != lpfnPreRequestHook)
	{
		lpfnPreRequestHook(
			lpRequest->rdInfo.hHttpServer,
			lpRequest->rdInfo.lpszRequestUri,
			lpRequest->rdInfo.hmType,
			lpPreRequestHookParam,
			&(lpRequest->rdInfo)
		);
	}

	if (NULLPTR != lpfnCallBack)
	{ 
		hscRet = lpfnCallBack(
			lpRequest->rdInfo.hHttpServer,
			lpRequest->rdInfo.lpszRequestUri,
			lpRequest->rdInfo.hmType,
			lpUserData,
			&(lpRequest->rdInfo)
		);
	}
	/* 404 */
	else 
	{
		hscRet = BuiltIn404(
			lpRequest->rdInfo.hHttpServer,
			lpRequest->rdInfo.lpszRequestUri,
			lpRequest->rdInfo.hmType,
			lpUserData,
			&(lpRequest->rdInfo)
		);
	}

	if (NULLPTR != lpfnPostRequestHook)
	{
		lpfnPostRequestHook(
			lpRequest->rdInfo.hHttpServer,
			lpRequest->rdInfo.lpszRequestUri,
			lpRequest->rdInfo.hmType,
			lpPostRequestHookParam,
			&(lpRequest->rdInfo)
		);
	}


	lpUserResponse = DynamicHeapRegionData(lpRequest->hTempResponsePayload, &ualUserResponseSize);

	/* Status Code And Content-Length */
	ZeroMemory(csTemp, sizeof(csTemp));
	HTTP_STATUS_CODE_TO_STRING(hscRet, lpszStatusMessage);

	StringPrintFormatSafe(
		csTemp,
		sizeof(csTemp) - 1,
		"%s %s\r\n",
		HTTP_VERSION_TO_STRING(lpRequest->rdInfo.htvType),
		lpszStatusMessage
	);

	/* Append the first line of info for the response */
	DynamicHeapRegionAppend(lpRequest->hResponsePayload, csTemp, StringLength(csTemp));

	ZeroMemory(csTemp, sizeof(csTemp));
	StringPrintFormatSafe(
		csTemp,
		sizeof(csTemp) - 1,
		"Content-Length: %lu",
		ualUserResponseSize
	);

	/* Append headers */
	RequestAddHeader(hRequest, (LPCSTR)csTemp);
	RequestAddHeader(hRequest, "X-Powered-By: CRock");
	RequestAddHeader(hRequest, "X-License: AGPLv3");
	RequestAddHeader(hRequest, "X-Source-Code: https://gitlab.com/zachpodbielniak/CRock");
	RequestAddHeader(hRequest, "X-Fueled-By: PodNet https://gitlab.com/zachpodbielniak/PodNet");
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	ZeroMemory(csTemp, sizeof(csTemp));

	onesdk_stub_get_version(&ossvInfo);
	StringPrintFormatSafe(
		csTemp,
		sizeof(csTemp) - 1,
		"X-OneSdk-Version: %u.%u.%u",
		ossvInfo.version_major,
		ossvInfo.version_minor,
		ossvInfo.version_patch
	);

	RequestAddHeader(hRequest, (LPCSTR)csTemp);

	ZeroMemory(csTemp, sizeof(csTemp));
	StringPrintFormatSafe(
		csTemp,
		sizeof(csTemp) - 1,
		"X-OneAgent-Version: %s",
		(LPCSTR)onesdk_agent_get_version_string()
	);

	RequestAddHeader(hRequest, (LPCSTR)csTemp);
	#endif
	
	/* Add User Header */
	lpUserHeader = DynamicHeapRegionData(lpRequest->hTempHeaderPayload, &ualUserHeaderSize);
	DynamicHeapRegionAppend(lpRequest->hResponsePayload, lpUserHeader, ualUserHeaderSize);
	DynamicHeapRegionAppend(lpRequest->hResponsePayload, "\r\n", 2); 


	if (0 != ualUserResponseSize)
	{ DynamicHeapRegionAppend(lpRequest->hResponsePayload, lpUserResponse, ualUserResponseSize); }

	lpResponse = DynamicHeapRegionData(lpRequest->hResponsePayload, &ualResponseSize);

	/* Ipv6 Response */
	if (HANDLE_TYPE_SOCKET6 == GetHandleDerivativeType(lpRequest->rdInfo.hClient))
	{
		SocketSend6(
			lpRequest->rdInfo.hClient,
			lpResponse,
			ualResponseSize
		);
	}
	/* Ipv4 Response */
	else
	{
		SocketSend4(
			lpRequest->rdInfo.hClient,
			lpResponse,
			ualResponseSize
		);
	}

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_incomingwebrequesttracer_set_status_code(
		lpRequest->othTracer,
		(onesdk_int32_t)hscRet
	);
	
	onesdk_tracer_end(lpRequest->othTracer);
	#endif

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
RequestAddHeader(
	_In_ 		HANDLE 		hRequest,
	_In_ 		LPCSTR 		lpcszHeader
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszHeader, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;
	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR lpszSplit;
	CSTRING csHeader[512];
	#endif

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_LIKELY_NULL(lpRequest, FALSE);

	DynamicHeapRegionAppend(lpRequest->hTempHeaderPayload, (LPVOID)lpcszHeader, StringLength(lpcszHeader));
	DynamicHeapRegionAppend(lpRequest->hTempHeaderPayload, "\r\n", 2);
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	ZeroMemory(csHeader, sizeof(csHeader));

	lpszSplit = StringInString(lpcszHeader, ":");
	if (NULLPTR == lpszSplit)
	{ JUMP(__DONT_ADD); }

	if (sizeof(csHeader) > ((UARCHLONG)lpszSplit - (UARCHLONG)lpcszHeader))
	{ CopyMemory(csHeader, lpcszHeader, ((UARCHLONG)lpszSplit) - (UARCHLONG)lpcszHeader); }
	else 
	{ JUMP(__DONT_ADD); }

	lpszSplit = (LPSTR)((UARCHLONG)lpszSplit + 0x01U);

	while (' ' == *lpszSplit)
	{ lpszSplit = (LPSTR)((UARCHLONG)lpszSplit + 0x01U); }

	onesdk_incomingwebrequesttracer_add_response_header(
		lpRequest->othTracer,
		onesdk_asciistr((LPCSTR)csHeader),
		onesdk_asciistr((LPCSTR)lpszSplit)
	);

	__DONT_ADD:
	#endif

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
RequestAddResponse(
	_In_ 		HANDLE 		hRequest,
	_In_ 		LPVOID 		lpData,
	_In_		UARCHLONG 	ualSize
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpData, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);

	return DynamicHeapRegionAppend(lpRequest->hTempResponsePayload, lpData, ualSize);
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API 
BOOL 
ProcessQueryStringParameters(
	_In_ 		HANDLE 		hRequest
){
	EXIT_IF_UNLIKELY_NULL(hRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRequest), HANDLE_TYPE_HTTP_SERVER_REQUEST, FALSE);

	LPREQUEST lpRequest;
	DLPSTR dlpszParams;
	DLPSTR dlpszIndex;
	UARCHLONG ualParamCount;
	UARCHLONG ualIndexCount;
	UARCHLONG ualIndex;
	UARCHLONG ualKeyLength;
	UARCHLONG ualValueLength;
	QUERYSTRING_DATA qsdData;

	lpRequest = OBJECT_CAST(hRequest, LPREQUEST);
	EXIT_IF_UNLIKELY_NULL(lpRequest, FALSE);

	if (NULLPTR != lpRequest->rdInfo.lpszRequestParamsDecoded)
	{
		ualParamCount = StringSplit(
			lpRequest->rdInfo.lpszRequestParamsDecoded,
			"&",
			&dlpszParams
		);
	}
	else 
	{ 
		ualParamCount = StringSplit(
			lpRequest->rdInfo.lpszRequestParams,
			"&",
			&dlpszParams
		);
	}

	if (0 == ualParamCount)
	{ 
		DestroySplitString(dlpszParams);
		return FALSE;
	}

	/* Split Up The Split String By '=' To Get L And R Values */
	for (
		ualIndex = 0;
		ualIndex < ualParamCount;
		ualIndex++
	){
		ualIndexCount = StringSplit(
			dlpszParams[ualIndex],
			"=",
			&dlpszIndex
		);

		if (2 != ualIndexCount)
		{
			DestroySplitString(dlpszIndex);
			DestroySplitString(dlpszParams);
			return FALSE;
		}

		ualKeyLength = StringLength(dlpszIndex[0]);
		ualValueLength = StringLength(dlpszIndex[1]);

		qsdData.lpszKey = GlobalAllocAndZero(ualKeyLength + 1);
		qsdData.lpszValue = GlobalAllocAndZero(ualValueLength + 1);

		CopyMemory(qsdData.lpszKey, dlpszIndex[0], ualKeyLength);
		CopyMemory(qsdData.lpszValue, dlpszIndex[1], ualValueLength);

		if (NULLPTR == lpRequest->rdInfo.hvQueryParameters)
		{ lpRequest->rdInfo.hvQueryParameters = CreateVector(8, sizeof(QUERYSTRING_DATA), 0); }

		VectorPushBack(lpRequest->rdInfo.hvQueryParameters, &qsdData, 0); 
		DestroySplitString(dlpszIndex);
	}
	
	DestroySplitString(dlpszParams);
	return TRUE;
}