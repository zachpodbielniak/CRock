/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_PREREQS_H
#define CROCK_PREREQS_H


#include <PodNet/PodNet.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CContainers/CHashTable.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CFile/CDirectory.h>
#include <PodNet/CMemory/CDynamicHeapRegion.h>
#include <PodNet/CNetworking/CIpv4.h>
#include <PodNet/CNetworking/CIpv6.h>
#include <PodNet/CNetworking/CSocket4.h>
#include <PodNet/CNetworking/CSocket6.h>
#include <PodNet/CPoller/CPoller.h>
#include <PodNet/CString/CHeapString.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CThread/CThread.h>
#include <ctype.h>


#endif