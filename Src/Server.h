/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_SERVER_H
#define CROCK_SERVER_H


#include "Defs.h"
#include "Request.h"

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif



typedef _Call_Back_ HTTP_STATUS_CODE (* LPFN_HTTP_URI_INVOCATION_PROC)(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
);

typedef LPFN_HTTP_URI_INVOCATION_PROC	*DLPFN_HTTP_URI_INVOCATION_PROC;




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServer(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServerEx(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize,
	_In_ 		UARCHLONG 		ualNumberOfJobThreads,
	_In_ 		ULONG 			ulFlags
);




#ifdef BUILD_WITH_ONEAGENT_SDK
_Success_(return != NULL_OBJECT, _Non_Locking_)
CROCK_API
HANDLE
CreateHttpServerExOneSdk(
	_In_Z_ 		LPCSTR 			lpcszName,
	_In_Z_ 		DLPCSTR 		dlpcszBindAddresses,
	_In_ 		UARCHLONG		ualNumberOfAddresses,
	_In_ 		LPUSHORT 		lpusPorts,
	_In_ 		UARCHLONG		ualNumberOfPorts,
	_In_Z_ 		LPCSTR 			lpcszHostName,
	_In_ 		UARCHLONG 		ualHashSize,
	_In_ 		UARCHLONG 		ualNumberOfJobThreads,
	_In_Z_ 		LPCSTR 			lpcszContextRoot,
	_In_Z_ 		LPCSTR 			lpcszTenantHostname,
	_In_ 		USHORT 			usTenantPort,
	_In_Z_ 		LPCSTR 			lpcszApiToken,
	_In_Z_ 		LPCSTR 			lpcszApplicationId,
	_In_ 		UARCHLONG		ualRumTagRefreshTime,
	_In_ 		ULONG 			ulFlags
);
#endif




_Success_(return != FALSE, _Acquires_Shared_Lock_)
CROCK_API
BOOL
DefineHttpUriProc(
	_In_ 		HANDLE 				hHttpServer,
	_In_Z_		LPCSTR 				lpcszUri,
	_In_ 		HTTP_METHOD			hmType,
	_In_ 		LPFN_HTTP_URI_INVOCATION_PROC	lpfnCallBack,
	_In_ 		LPVOID 				lpUserData
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_)
CROCK_API
LPFN_HTTP_URI_INVOCATION_PROC
GetHttpUriProc(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_Out_Opt_	DLPVOID 		dlpUserData
);




_Success_(return != FALSE, _Acquires_Shared_Lock_)
CROCK_API 
BOOL 
SetHttpServerHooks(
	_In_		HANDLE 				hHttpServer,
	_In_Opt_ 	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPreRequestHook,
	_In_Opt_ 	LPVOID 				lpPreRequestHookParam,
	_In_Opt_ 	LPFN_HTTP_URI_INVOCATION_PROC	lpfnPostRequestHook,
	_In_Opt_ 	LPVOID 				lpPostRequestHookParam
);




_Success_(return != FALSE, _Interlocked_Operation_)
CROCK_API 
BOOL 
GetHttpServerHooks(
	_In_ 		HANDLE 				hHttpServer,
	_Out_Opt_ 	DLPFN_HTTP_URI_INVOCATION_PROC	dlpfnPreRequestHook,
	_Out_Opt_ 	DLPVOID 			dlpPreRequestHookParam,
	_Out_Opt_ 	DLPFN_HTTP_URI_INVOCATION_PROC	dlpfnPostRequestHook,
	_Out_Opt_ 	DLPVOID 			dlpPostRequestHookParam
);




#ifdef BUILD_WITH_ONEAGENT_SDK
_Success_(return != ONESDK_INVALID_HANDLE, _Interlocked_Operation_)
CROCK_API
onesdk_webapplicationinfo_handle_t
GetHttpServerWebAppHandle(
	_In_ 		HANDLE 			hHttpServer
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
CROCK_API
HSTRING
GetRumScriptTag(
	_In_ 		HANDLE 			hHttpServer
);
#endif



#endif