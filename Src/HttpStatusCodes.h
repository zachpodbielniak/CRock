/*


  ____ ____            _    
 / ___|  _ \ ___   ___| | __
| |   | |_) / _ \ / __| |/ /
| |___|  _ < (_) | (__|   < 
 \____|_| \_\___/ \___|_|\_\

C Micro-Framework Library Similar To Python's Flask.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CROCK_HTTPSTATUSCODES_H
#define CROCK_HTTPSTATUSCODES_H


typedef enum __HTTP_STATUS_CODE
{
	HTTP_STATUS_100_CONTINUE				= 100,
	HTTP_STATUS_101_SWITCHING_PROTOCOLS			= 101,
	HTTP_STATUS_102_PROCESSING				= 102,
	HTTP_STATUS_103_EARLY_HINTS 				= 103,

	HTTP_STATUS_200_OK					= 200,
	HTTP_STATUS_201_CREATED 				= 201,
	HTTP_STATUS_202_ACCEPTED 				= 202,
	HTTP_STATUS_203_NON_AUTHORITIVE_INFORMATION		= 203,
	HTTP_STATUS_204_NO_CONTENT				= 204,
	HTTP_STATUS_205_RESET_CONTENT				= 205,
	HTTP_STATUS_206_PARTIAL_CONTENT				= 206,
	HTTP_STATUS_207_MULTI_STATUS				= 207,
	HTTP_STATUS_208_ALREADY_REPORTED			= 208,
	HTTP_STATUS_226_IM_USED					= 226,

	HTTP_STATUS_300_MULTIPLE_CHOICES			= 300,
	HTTP_STATUS_301_MOVED_PERMANENTLY			= 301,
	HTTP_STATUS_302_FOUND					= 302,
	HTTP_STATUS_303_SEE_OTHER 				= 303,
	HTTP_STATUS_304_NOT_MODIFIED				= 304,
	HTTP_STATUS_305_USE_PROXY				= 305,
	HTTP_STATUS_306_SWITCH_PROXY				= 306,
	HTTP_STATUS_307_TEMPORARY_REDIRECT			= 307,
	HTTP_STATUS_308_PERMANENT_REDIRECT			= 308,

	HTTP_STATUS_400_BAD_REQUEST				= 400,
	HTTP_STATUS_401_UNAUTHORIZED				= 401,
	HTTP_STATUS_402_PAYMENT_REQUIRED			= 402,
	HTTP_STATUS_403_FORBIDDEN				= 403,
	HTTP_STATUS_404_NOT_FOUND				= 404,
	HTTP_STATUS_405_METHOD_NOT_ALLOWED			= 405,
	HTTP_STATUS_406_NOT_ACCEPTABLE				= 406,
	HTTP_STATUS_407_PROXY_AUTHENTICATION_REQUIRED		= 407,
	HTTP_STATUS_408_REQUEST_TIMEOUT				= 408,
	HTTP_STATUS_409_CONFLICT				= 409,
	HTTP_STATUS_410_GONE					= 410,
	HTTP_STATUS_411_LENGTH_REQUIRED				= 411,
	HTTP_STATUS_412_PRECONDITION_FAILED			= 412,
	HTTP_STATUS_413_PAYLOAD_REQURED				= 413,
	HTTP_STATUS_414_URI_TOO_LONG				= 414,
	HTTP_STATUS_415_UNSUPPORTED_MEDIA_TYPE			= 415,
	HTTP_STATUS_416_RANGE_NOT_SATISFIABLE			= 416,
	HTTP_STATUS_417_EXPECTATION_FAILED			= 417,
	HTTP_STATUS_418_IM_A_TEAPOT				= 418,
	HTTP_STATUS_421_MISDIRECTED_REQUEST			= 421,
	HTTP_STATUS_422_UNPROCESSABLE_ENTITY			= 422,
	HTTP_STATUS_423_LOCKED					= 423,
	HTTP_STATUS_424_FAILED_DEPENDENCY			= 424,
	HTTP_STATUS_425_TOO_EARLY				= 425,
	HTTP_STATUS_426_UPGRADE_REQUIRED			= 426,
	HTTP_STATUS_428_PRECONDITION_REQUIRED			= 428,
	HTTP_STATUS_429_TOO_MANY_REQUESTS			= 429,
	HTTP_STATUS_431_REQUEST_HEADER_FIELDS_TOO_LARGE		= 431,
	HTTP_STATUS_451_UNAVAILABLE_FOR_LEGAL_REASONS		= 451,

	HTTP_STATUS_500_INTERNAL_SERVER_ERROR			= 500,
	HTTP_STATUS_501_NOT_IMPLEMENTED				= 501,
	HTTP_STATUS_502_BAD_GATEWAY				= 502,
	HTTP_STATUS_503_SERVICE_UNAVAILABLE			= 503,
	HTTP_STATUS_504_GATEWAY_TIMEOUT				= 504,
	HTTP_STATUS_505_HTTP_VERSION_NOT_SUPPORTED		= 505,
	HTTP_STATUS_506_VARIANT_ALSO_NEGOTIATES			= 506,
	HTTP_STATUS_507_INSUFFICIENT_STORAGE			= 507,
	HTTP_STATUS_508_LOOP_DETECTED				= 508,
	HTTP_STATUS_510_NOT_EXTENDED				= 510,
	HTTP_STATUS_511_NETWORK_AUTHENTICATION_REQUIRED		= 511

} HTTP_STATUS_CODE;



#define HTTP_STATUS_CODE_TO_STRING(X,O)					\
	switch ((HTTP_STATUS_CODE)(X))					\
	{								\
		case HTTP_STATUS_100_CONTINUE:				\
		{							\
			(O) = "100 Continue"; 				\
			break;						\
		}							\
		case HTTP_STATUS_101_SWITCHING_PROTOCOLS:		\
		{							\
			(O) = "101 Switching Protocols";		\
			break;						\
		}							\
		case HTTP_STATUS_102_PROCESSING:			\
		{							\
			(O) = "102 Processing"; 			\
			break;						\
		}							\
		case HTTP_STATUS_103_EARLY_HINTS:			\
		{							\
			(O) = "103 Early Hints"; 			\
			break;						\
		}							\
		case HTTP_STATUS_200_OK:				\
		{							\
			(O) = "200 OK"; 				\
			break;						\
		}							\
		case HTTP_STATUS_201_CREATED:				\
		{							\
			(O) = "201 Created"; 				\
			break;						\
		}							\
		case HTTP_STATUS_202_ACCEPTED:				\
		{							\
			(O) = "202 Accepted"; 				\
			break;						\
		}							\
		case HTTP_STATUS_203_NON_AUTHORITIVE_INFORMATION:	\
		{							\
			(O) = "203 Non-Authoritative Information"; 	\
			break;						\
		}							\
		case HTTP_STATUS_204_NO_CONTENT:			\
		{							\
			(O) = "204 No Content"; 			\
			break;						\
		}							\
		case HTTP_STATUS_205_RESET_CONTENT:			\
		{							\
			(O) = "205 Reset Content"; 			\
			break;						\
		}							\
		case HTTP_STATUS_206_PARTIAL_CONTENT:			\
		{							\
			(O) = "206 Partial Content"; 			\
			break;						\
		}							\
		case HTTP_STATUS_207_MULTI_STATUS:			\
		{							\
			(O) = "207 Multi-Status"; 			\
			break;						\
		}							\
		case HTTP_STATUS_208_ALREADY_REPORTED:			\
		{							\
			(O) = "208 Already Reported"; 			\
			break;						\
		}							\
		case HTTP_STATUS_226_IM_USED:				\
		{							\
			(O) = "226 IM Used"; 				\
			break;						\
		}							\
		case HTTP_STATUS_300_MULTIPLE_CHOICES:			\
		{							\
			(O) = "300 Multiple Choices"; 			\
			break;						\
		}							\
		case HTTP_STATUS_301_MOVED_PERMANENTLY:			\
		{							\
			(O) = "301 Moved Permanently"; 			\
			break;						\
		}							\
		case HTTP_STATUS_302_FOUND:				\
		{							\
			(O) = "302 Found"; 				\
			break;						\
		}							\
		case HTTP_STATUS_303_SEE_OTHER:				\
		{							\
			(O) = "303 See Other"; 				\
			break;						\
		}							\
		case HTTP_STATUS_304_NOT_MODIFIED:			\
		{							\
			(O) = "304 Not Modified"; 			\
			break;						\
		}							\
		case HTTP_STATUS_305_USE_PROXY:				\
		{							\
			(O) = "305 Use Proxy"; 				\
			break;						\
		}							\
		case HTTP_STATUS_306_SWITCH_PROXY:			\
		{							\
			(O) = "306 Switch Proxy"; 			\
			break;						\
		}							\
		case HTTP_STATUS_307_TEMPORARY_REDIRECT:		\
		{							\
			(O) = "307 Temporary Redirect"; 		\
			break;						\
		}							\
		case HTTP_STATUS_308_PERMANENT_REDIRECT:		\
		{							\
			(O) = "308 Permanent Redirect"; 		\
			break;						\
		}							\
		case HTTP_STATUS_400_BAD_REQUEST:			\
		{							\
			(O) = "400 Bad Request"; 			\
			break;						\
		}							\
		case HTTP_STATUS_401_UNAUTHORIZED:			\
		{							\
			(O) = "401 Unauthorized"; 			\
			break;						\
		}							\
		case HTTP_STATUS_402_PAYMENT_REQUIRED:			\
		{							\
			(O) = "402 Payment Required"; 			\
			break;						\
		}							\
		case HTTP_STATUS_403_FORBIDDEN:				\
		{							\
			(O) = "403 Forbidden"; 				\
			break;						\
		}							\
		case HTTP_STATUS_404_NOT_FOUND:				\
		{							\
			(O) = "404 Not Found"; 				\
			break;						\
		}							\
		case HTTP_STATUS_405_METHOD_NOT_ALLOWED:		\
		{							\
			(O) = "405 Method Not Allowed"; 		\
			break;						\
		}							\
		case HTTP_STATUS_406_NOT_ACCEPTABLE:			\
		{							\
			(O) = "406 Not Acceptable"; 			\
			break;						\
		}							\
		case HTTP_STATUS_407_PROXY_AUTHENTICATION_REQUIRED:	\
		{							\
			(O) = "407 Proxy Authentication Required"; 	\
			break;						\
		}							\
		case HTTP_STATUS_408_REQUEST_TIMEOUT:			\
		{							\
			(O) = "408 Request Timeout"; 			\
			break;						\
		}							\
		case HTTP_STATUS_409_CONFLICT:				\
		{							\
			(O) = "409 Conflict"; 				\
			break;						\
		}							\
		case HTTP_STATUS_410_GONE:				\
		{							\
			(O) = "410 Gone"; 				\
			break;						\
		}							\
		case HTTP_STATUS_411_LENGTH_REQUIRED:			\
		{							\
			(O) = "411 Length Required"; 			\
			break;						\
		}							\
		case HTTP_STATUS_412_PRECONDITION_FAILED:		\
		{							\
			(O) = "Precondition Failed"; 			\
			break;						\
		}							\
		case HTTP_STATUS_413_PAYLOAD_REQURED:			\
		{							\
			(O) = "413 Payload Required"; 			\
			break;						\
		}							\
		case HTTP_STATUS_414_URI_TOO_LONG:			\
		{							\
			(O) = "414 URI Too Long"; 			\
			break;						\
		}							\
		case HTTP_STATUS_415_UNSUPPORTED_MEDIA_TYPE:		\
		{							\
			(O) = "415 Unsupported Media Type"; 		\
			break;						\
		}							\
		case HTTP_STATUS_416_RANGE_NOT_SATISFIABLE:		\
		{							\
			(O) = "416 Range Not Satisfiable"; 		\
			break;						\
		}							\
		case HTTP_STATUS_417_EXPECTATION_FAILED:		\
		{							\
			(O) = "417 Expectation Failed"; 		\
			break;						\
		}							\
		case HTTP_STATUS_418_IM_A_TEAPOT:			\
		{							\
			(O) = "418 I'm a teapot"; 			\
			break;						\
		}							\
		case HTTP_STATUS_421_MISDIRECTED_REQUEST:		\
		{							\
			(O) = "421 Misdirected Request"; 		\
			break;						\
		}							\
		case HTTP_STATUS_422_UNPROCESSABLE_ENTITY:		\
		{							\
			(O) = "422 Unprocessable Entity"; 		\
			break;						\
		}							\
		case HTTP_STATUS_423_LOCKED:				\
		{							\
			(O) = "423 Locked"; 				\
			break;						\
		}							\
		case HTTP_STATUS_424_FAILED_DEPENDENCY:			\
		{							\
			(O) = "424 Failed Dependency"; 			\
			break;						\
		}							\
		case HTTP_STATUS_425_TOO_EARLY:				\
		{							\
			(O) = "425 Too Early"; 				\
			break;						\
		}							\
		case HTTP_STATUS_426_UPGRADE_REQUIRED:			\
		{							\
			(O) = "426 Upgrade Required"; 			\
			break;						\
		}							\
		case HTTP_STATUS_428_PRECONDITION_REQUIRED:		\
		{							\
			(O) = "428 Precondition Required"; 		\
			break;						\
		}							\
		case HTTP_STATUS_429_TOO_MANY_REQUESTS:			\
		{							\
			(O) = "429 Too Many Requests"; 			\
			break;						\
		}							\
		case HTTP_STATUS_431_REQUEST_HEADER_FIELDS_TOO_LARGE:	\
		{							\
			(O) = "431 Request Header Fields Too Large"; 	\
			break;						\
		}							\
		case HTTP_STATUS_451_UNAVAILABLE_FOR_LEGAL_REASONS:	\
		{							\
			(O) = "451 Unavailable For Legal Reasons"; 	\
			break;						\
		}							\
		case HTTP_STATUS_500_INTERNAL_SERVER_ERROR:		\
		{							\
			(O) = "500 Internal Server Error"; 		\
			break;						\
		}							\
		case HTTP_STATUS_501_NOT_IMPLEMENTED:			\
		{							\
			(O) = "501 Not Implemented"; 			\
			break;						\
		}							\
		case HTTP_STATUS_502_BAD_GATEWAY:			\
		{							\
			(O) = "502 Bad Gateway"; 			\
			break;						\
		}							\
		case HTTP_STATUS_503_SERVICE_UNAVAILABLE:		\
		{							\
			(O) = "503 Service Unavailable"; 		\
			break;						\
		}							\
		case HTTP_STATUS_504_GATEWAY_TIMEOUT:			\
		{							\
			(O) = "504 Gateway Timeout"; 			\
			break;						\
		}							\
		case HTTP_STATUS_505_HTTP_VERSION_NOT_SUPPORTED:	\
		{							\
			(O) = "505 HTTP Version Not Supported"; 	\
			break;						\
		}							\
		case HTTP_STATUS_506_VARIANT_ALSO_NEGOTIATES:		\
		{							\
			(O) = "506 Variant Also Negotiates"; 		\
			break;						\
		}							\
		case HTTP_STATUS_507_INSUFFICIENT_STORAGE:		\
		{							\
			(O) = "507 Insufficient Storage"; 		\
			break;						\
		}							\
		case HTTP_STATUS_508_LOOP_DETECTED:			\
		{							\
			(O) = "508 Loop Detected"; 			\
			break;						\
		}							\
		case HTTP_STATUS_510_NOT_EXTENDED:			\
		{							\
			(O) = "510 Not Extended"; 			\
			break;						\
		}							\
		case HTTP_STATUS_511_NETWORK_AUTHENTICATION_REQUIRED:	\
		{							\
			(O) = "511 Network Authentication Required"; 	\
			break;						\
		}							\
		default:						\
		{							\
			(O) = "500 Internal Server Error";		\
			break;						\
		}							\
	}								



#endif