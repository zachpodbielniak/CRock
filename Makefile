#   ____ ____            _    
#  / ___|  _ \ ___   ___| | __
# | |   | |_) / _ \ / __| |/ /
# | |___|  _ < (_) | (__|   < 
#  \____|_| \_\___/ \___|_|\_\

# Network Message Queue In C.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE
ifeq ($(BUILD_TYPE),onesdk)
DEFINES += -D BUILD_WITH_ONEAGENT_SDK
endif

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__
ifeq ($(BUILD_TYPE),onesdk)
DEFINES_D += -D BUILD_WITH_ONEAGENT_SDK
endif

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall



FILES = Request.o Server.o Post.o BuiltInHandlers.o Load.o Url.o StaticContent.o
FILES_D = Request_d.o Server_d.o Post_d.o BuiltInHandlers_d.o Load_d.o Url_d.o StaticContent_d.o




all:	bin libcrock.so 
debug:	bin libcrock_d.so
static-content-server: bin scs
static-content-server_d: bin scs_d
test: 	bin $(TEST)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

check:	bin 
	cppcheck . --std=c89 -j $(shell nproc) --inline-suppr --error-exitcode=1



# Install

install:
	cp bin/libcrock.so /usr/lib/

install_debug:
	cp bin/libcrock_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/CRock/
	mkdir -p /usr/include/CRock
	find . -type f -name "*.h" -exec install -D {} /usr/include/CRock/{} \;
	mv /usr/include/CRock/Src/* /usr/include/CRock
	rm -rf /usr/include/CRock/Src



libcrock.so: $(FILES)
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -shared -fPIC -s -o bin/libcrock.so bin/*.o -pthread -lpodnet -lonesdk_shared -ldl -lmagic $(STD) $(OPTIMIZE) $(MARCH)
else
	$(CC) -shared -fPIC -s -o bin/libcrock.so bin/*.o -pthread -lpodnet -lmagic $(STD) $(OPTIMIZE) $(MARCH)
endif
	rm bin/*.o

libcrock_d.so: $(FILES_D)
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -g -shared -fPIC -o bin/libcrock_d.so bin/*_d.o -pthread -lpodnet_d -ldl -lonesdk_shared -lmagic $(STD)
else
	$(CC) -g -shared -fPIC -o bin/libcrock_d.so bin/*_d.o -pthread -lpodnet_d -lmagic $(STD)
endif
	rm bin/*.o

test:
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -g -fPIC -o bin/test Tests/Main.c -lpodnet_d -lcrock_d -lonesdk_shared -ldl -lmagic $(CC_FLAGS_T)
else
	$(CC) -g -fPIC -o bin/test Tests/Main.c -lpodnet_d -lcrock_d -lmagic $(CC_FLAGS_T)
endif

test_prod:
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -fPIC -o bin/test Tests/Main.c -lpodnet -lcrock -lonesdk_shared -ldl -lmagic $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH)
else
	$(CC) -fPIC -o bin/test Tests/Main.c -lpodnet -lcrock -lmagic $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH)
endif

scs:
	$(CC) -fPIC -o bin/static-content-server Tools/StaticContentServer.c -lcrock -lpodnet -lmagic $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH)

scs_d:
	$(CC) -g -fPIC -o bin/static-content-server Tools/StaticContentServer.c -lcrock_d -lpodnet_d -lmagic $(CC_FLAGS_D)



Request.o: 
	$(CC) -fPIC -c -o bin/Request.o Src/Request.c $(CC_FLAGS)

Server.o:
	$(CC) -fPIC -c -o bin/Server.o Src/Server.c $(CC_FLAGS)

Post.o:	
	$(CC) -fPIC -c -o bin/Post.o Src/Post.c $(CC_FLAGS)

BuiltInHandlers.o:
	$(CC) -fPIC -c -o bin/BuiltInHandlers.o Src/BuiltInHandlers.c $(CC_FLAGS)

Load.o:	
	$(CC) -fPIC -c -o bin/Load.o Src/Load.c $(CC_FLAGS)

Url.o:	
	$(CC) -fPIC -c -o bin/Url.o Src/Url.c $(CC_FLAGS)

StaticContent.o:
	$(CC) -fPIC -c -o bin/StaticContent.o Src/StaticContent.c $(CC_FLAGS)





Request_d.o: 
	$(CC) -g -fPIC -c -o bin/Request_d.o Src/Request.c $(CC_FLAGS_D)

Server_d.o:
	$(CC) -g -fPIC -c -o bin/Server_d.o Src/Server.c $(CC_FLAGS_D)

Post_d.o:	
	$(CC) -g -fPIC -c -o bin/Post_d.o Src/Post.c $(CC_FLAGS_D)

BuiltInHandlers_d.o:
	$(CC) -g -fPIC -c -o bin/BuiltInHandlers_d.o Src/BuiltInHandlers.c $(CC_FLAGS_D)

Load_d.o:	
	$(CC) -g -fPIC -c -o bin/Load_d.o Src/Load.c $(CC_FLAGS_D)

Url_d.o:
	$(CC) -g -fPIC -c -o bin/Url_d.o Src/Url.c $(CC_FLAGS_D)

StaticContent_d.o:
	$(CC) -g -fPIC -c -o bin/StaticContent_d.o Src/StaticContent.c $(CC_FLAGS_D)